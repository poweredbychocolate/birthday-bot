## Birthday bot for Discord

###  Description

Birthday bot, simple java app that connect with discord bot application using token [see](https://discord.com/developers/applications).
User can add, edit or remove date of his birthday by send command via discord message.
App allow exporting birthdays as PDF calendar file, this can be done via command message also.
Furthermore, user with administration permission can remove birthday record manually.

Application automatically, once at day send information message on a selected channel with user or users celebrating today his birthday.
Administration user can do this manually by a command message also.
Username or nickname will be updated after changes in discord account.
Additionally, saved user record will be disabled on leave server and enable again if user back.

### How to run
For first run add additional **gen-config** argument, this gen example config file and message templates.
For example: 
>java -jar birthday-bot.jar gen-config

App can be run with selected locale by add it as argument. Skip to use default
For example to run with polish locale:
>java -jar birthday-bot.jar pl_PL

### How to configure

1. Edit *app-config.yml* to customise bot settings
 
```yaml
botToken: "token"
notificationChannelId: channel-id
birthdayCheckHour: hour
birthdayCheckMinute: minute
adminUserIds:
  - users-id-list
``` 

* `token` - discord bot access token.
* `channel-id` - channel id that will be used to send birthday info messages.
* `hour and minute` - day time for start birthday notification thread
* `users-id-list` - user ids list, that control access to administration commands

2. Edit `single-user-template.txt` to customise notification message sent when only one user has birthday 
3. Edit `multi-user-template.txt` to customise notification message for few user birthday
4. Copy `src/main/resources/lang.properties` and add locale to file name to customise bot command

### Template tags
* `${user.name}` - single tag, in both templates paste username or nickname if available.
* `${user.age}` - single tag, in both templates paste user age
* `${if.age}` - work with pairs, in both templates text and/or age tag between will be displayed only if user save birthday with year
* `${loop}` - work with pairs, only supported by **multi-user** template, text and/or other tags between will be repeated for every user birthday

### Commands and responses text
App use resource bundle lang to provide command and responses text customisation.  
For describe command list lang prefix is used, so this point to properties key. 

### Commands list
 * `lang.command.birthday.add day.month` - save user birthday, without year age not be calculated.
 * `lang.command.birthday.add day.month.year` - save user birthday.
---
 * `lang.command.birthday.who month(1-12)` - send response message with users that has birthday in given month.
 * `lang.command.birthday.who year` - as above, but for given year.
 * `lang.command.birthday.who day.month.year` - users for given date.
 * `lang.command.birthday.who month.year` - birthdays from given year and selected month.
 * `lang.command.birthday.who day.month` - last one allow get who has a birthday in selected day of given month.
---
 * `lang.command.birthday.remove` - remove birthday record if exists.
 * `lang.command.calendar` - start generating calendar, will be returned as message attachment.
---                                          
 * `lang.command.admin.remove.user userToRemoveId` - remove user birthday record for given ID. (only for administration users)
 * `lang.command.admin.birthday.thread.start` - manually run thread that checks if any user has birthday. (only for administration users)
### To do in a feature releases
* implement support for multiple discord servers
---
### Helpful Links

* [Discord developer portal](https://discord.com/developers/applications)
* [javacord](https://javacord.org/wiki/)
* [itext](https://kb.itextpdf.com/home/it7kb/examples)