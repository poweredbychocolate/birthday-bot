FROM maven:3-openjdk-11 AS maven

RUN mkdir /app
RUN mkdir /app/sources

COPY src /app/sources/src
COPY pom.xml /app/sources

RUN mvn clean install -f /app/sources/pom.xml

FROM adoptopenjdk:11-jre-hotspot
RUN mkdir /app
RUN mkdir /app/logs
VOLUME /app/logs

COPY --from=maven /app/sources/target/libraries  /app/libraries
COPY --from=maven /app/sources/target/birthday-bot-1.0.0-nightmare.jar  /app/birthday-bot.jar
COPY app-config.yml /app/app-config.yml
COPY multi-user-template.txt /app/multi-user-template.txt
COPY single-user-template.txt /app/single-user-template.txt
COPY src/main/resources/fonts/LiberationSans.ttf /app/src/main/resources/fonts/LiberationSans.ttf

WORKDIR /app
#run with default launguage
CMD java -jar birthday-bot.jar
#or run with prefered locale, for example pl
#CMD java -jar birthday-bot.jar pl-PL