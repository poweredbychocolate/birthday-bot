package pl.poweredbychocolate.adapter;

import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.poweredbychocolate.birthday.domain.Birthday;
import pl.poweredbychocolate.birthday.domain.BirthdayDate;
import pl.poweredbychocolate.birthday.domain.BirthdayUser;
import pl.poweredbychocolate.storage.domain.YamlFile;
import pl.poweredbychocolate.storage.repository.StorageRepository;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class StorageBirthdayRepositoryAdapterTest {

  @Mock
  private StorageRepository storageRepository;

  @InjectMocks
  private StorageBirthdayRepositoryAdapter storageBirthdayRepositoryAdapter;

  @Test
  @Order(1)
  void saveTest() throws IOException {

    doNothing().when(storageRepository).saveYamlToFile(any(YamlFile.class), any(Path.class));
    when(storageRepository.isFileExist(any(Path.class))).thenReturn(true);
    when(storageRepository.readYAMLFromFile(any(Path.class), any()))
        .thenReturn(new BirthdayStorage(new LinkedHashMap<>()));

    StorageBirthdayRepositoryAdapter localStorageRepository = new StorageBirthdayRepositoryAdapter(storageRepository);

    Birthday birthday = Birthday.builder()
        .user(BirthdayUser.builder()
            .userId(10L)
            .userName("test user 2")
            .build())
        .date(BirthdayDate.builder()
            .day(10)
            .month(5)
            .year(2000)
            .build())
        .build();

    assertDoesNotThrow(() -> {
      localStorageRepository.save(birthday);
      localStorageRepository.save(birthday);
    });

    verify(storageRepository).saveYamlToFile(any(YamlFile.class), any(Path.class));
    verify(storageRepository, times(2)).isFileExist(any(Path.class));
    verify(storageRepository).readYAMLFromFile(any(Path.class), any());
  }

  @Test
  @Order(2)
  void replaceTest() throws IOException {
    doNothing().when(storageRepository).saveYamlToFile(any(YamlFile.class), any(Path.class));

    assertDoesNotThrow(() -> {

      Birthday birthday = Birthday.builder()
          .user(BirthdayUser.builder()
              .userId(100L)
              .userName("test user 2")
              .build())
          .date(BirthdayDate.builder()
              .day(10)
              .month(5)
              .year(2000)
              .build())
          .build();
      storageBirthdayRepositoryAdapter.save(birthday);

      birthday = Birthday.builder()
          .user(BirthdayUser.builder()
              .userId(100L)
              .userName("test user 345")
              .build())
          .date(BirthdayDate.builder()
              .day(11)
              .month(10)
              .year(2006)
              .build())
          .build();

      storageBirthdayRepositoryAdapter.replace(birthday);

      birthday = Birthday.builder()
          .user(BirthdayUser.builder()
              .userId(200L)
              .userName("test user 345")
              .build())
          .date(BirthdayDate.builder()
              .day(11)
              .month(10)
              .year(2006)
              .build())
          .build();

      storageBirthdayRepositoryAdapter.replace(birthday);

    });

    verify(storageRepository, times(2)).saveYamlToFile(any(YamlFile.class), any(Path.class));
  }

  @Test
  @Order(3)
  void deleteTest() throws IOException {
    doNothing().when(storageRepository).saveYamlToFile(any(YamlFile.class), any(Path.class));

    assertDoesNotThrow(() -> {

      Birthday birthday = Birthday.builder()
          .user(BirthdayUser.builder()
              .userId(1000L)
              .userName("test user 2")
              .build())
          .date(BirthdayDate.builder()
              .day(10)
              .month(5)
              .year(2000)
              .build())
          .build();
      storageBirthdayRepositoryAdapter.save(birthday);
      storageBirthdayRepositoryAdapter.delete(birthday.getUser());
      storageBirthdayRepositoryAdapter.delete(birthday.getUser());

    });

    verify(storageRepository, times(2)).saveYamlToFile(any(YamlFile.class), any(Path.class));
  }

  @Test
  @Order(5)
  void findByBirthdayDateTest() throws IOException {
    doNothing().when(storageRepository).saveYamlToFile(any(YamlFile.class), any(Path.class));

    assertDoesNotThrow(() -> {
      for (Birthday birthday : prepareItemList()) {
        storageBirthdayRepositoryAdapter.save(birthday);
      }

      BirthdayDate inputBirthdayDate = BirthdayDate.builder().build();
      List<Birthday> resultList = storageBirthdayRepositoryAdapter.findBirthdayByBirthdayDateIfEnabled(inputBirthdayDate);
      assertEquals(3, resultList.size());

      inputBirthdayDate = BirthdayDate.builder()
          .day(1)
          .month(1)
          .year(1999)
          .build();
      resultList = storageBirthdayRepositoryAdapter.findBirthdayByBirthdayDateIfEnabled(inputBirthdayDate);
      assertEquals(0, resultList.size());

      inputBirthdayDate = BirthdayDate.builder()
          .day(11)
          .month(10)
          .build();

      resultList = storageBirthdayRepositoryAdapter.findBirthdayByBirthdayDateIfEnabled(inputBirthdayDate);
      assertEquals(2, resultList.size());

      assertTrue(resultList.stream()
          .noneMatch(birthday -> birthday.getUserId().equals(40L)));
    });

    verify(storageRepository, times(4)).saveYamlToFile(any(YamlFile.class), any(Path.class));
  }

  @Test
  @Order(4)
  void findAllTest() throws IOException {
    doNothing().when(storageRepository).saveYamlToFile(any(YamlFile.class), any(Path.class));

    assertDoesNotThrow(() -> {

      List<Birthday> resultList = storageBirthdayRepositoryAdapter.findAllEnabled();
      assertTrue(resultList.isEmpty());

      for (Birthday birthday : prepareItemList()) {
        storageBirthdayRepositoryAdapter.save(birthday);
      }

      resultList = storageBirthdayRepositoryAdapter.findAllEnabled();
      assertEquals(3, resultList.size());

    });

    verify(storageRepository, times(4)).saveYamlToFile(any(YamlFile.class), any(Path.class));
  }

  @Test
  @Order(6)
  void findByIdTest() throws IOException {
    doNothing().when(storageRepository).saveYamlToFile(any(YamlFile.class), any(Path.class));

    assertDoesNotThrow(() -> {

      Birthday birthday = Birthday.builder()
          .user(BirthdayUser.builder()
              .userId(1000L)
              .userName("test user 2")
              .build())
          .date(BirthdayDate.builder()
              .day(10)
              .month(5)
              .year(2000)
              .build())
          .build();

      var birthdayQueryResult = storageBirthdayRepositoryAdapter.findById(birthday.getUserId());
      assertTrue(birthdayQueryResult.isEmpty());

      storageBirthdayRepositoryAdapter.save(birthday);
      birthdayQueryResult = storageBirthdayRepositoryAdapter.findById(birthday.getUserId());

      assertTrue(birthdayQueryResult.isPresent());
      assertEquals(birthday, birthdayQueryResult.get());

    });

  }
  @Test
  @Order(3)
  void deleteByIdTest() throws IOException {
    doNothing().when(storageRepository).saveYamlToFile(any(YamlFile.class), any(Path.class));

    assertDoesNotThrow(() -> {

      Birthday birthday = Birthday.builder()
          .user(BirthdayUser.builder()
              .userId(1000L)
              .userName("test user 2")
              .build())
          .date(BirthdayDate.builder()
              .day(10)
              .month(5)
              .year(2000)
              .build())
          .build();
      storageBirthdayRepositoryAdapter.save(birthday);
      storageBirthdayRepositoryAdapter.deleteById(birthday.getUser().getUserId());
      storageBirthdayRepositoryAdapter.deleteById(birthday.getUser().getUserId());

    });

    verify(storageRepository, times(2)).saveYamlToFile(any(YamlFile.class), any(Path.class));
  }

  private List<Birthday> prepareItemList() {
    List<Birthday> itemList = new ArrayList<>();

    itemList.add(Birthday.builder()
        .user(BirthdayUser.builder()
            .userId(10L)
            .userName("test user 2")
            .enabled(true)
            .build())
        .date(BirthdayDate.builder()
            .day(10)
            .month(5)
            .year(2000)
            .build())
        .build());

    itemList.add(Birthday.builder()
        .user(BirthdayUser.builder()
            .userId(20L)
            .userName("test user 345")
            .enabled(true)
            .build())
        .date(BirthdayDate.builder()
            .day(11)
            .month(10)
            .year(2007)
            .build())
        .build());

    itemList.add(Birthday.builder()
        .user(BirthdayUser.builder()
            .userId(30L)
            .userName("test user 345")
            .enabled(true)
            .build())
        .date(BirthdayDate.builder()
            .day(11)
            .month(10)
            .year(2006)
            .build())
        .build());

    itemList.add(Birthday.builder()
        .user(BirthdayUser.builder()
            .userId(40L)
            .userName("test user 345")
            .enabled(false)
            .build())
        .date(BirthdayDate.builder()
            .day(11)
            .month(10)
            .year(2006)
            .build())
        .build());
    return itemList;
  }
}