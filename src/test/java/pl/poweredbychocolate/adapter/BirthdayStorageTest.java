package pl.poweredbychocolate.adapter;

import org.junit.jupiter.api.Test;
import pl.poweredbychocolate.birthday.domain.Birthday;
import pl.poweredbychocolate.birthday.domain.BirthdayDate;
import pl.poweredbychocolate.birthday.domain.BirthdayUser;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

class BirthdayStorageTest {

  @Test
  void addTest() {
    final BirthdayStorage birthdayStorage = new BirthdayStorage(new LinkedHashMap<>());
    Birthday birthday = Birthday.builder()
        .user(BirthdayUser.builder()
            .userId(1L)
            .userName("drunk developer")
            .build())
        .date(BirthdayDate.builder()
            .day(20)
            .month(12)
            .year(2015)
            .build())
        .build();

    assertTrue(birthdayStorage.add(birthday.getUserId(), birthday));
    assertFalse(birthdayStorage.add(birthday.getUserId(), birthday));

    birthday = Birthday.builder()
        .user(BirthdayUser.builder()
            .userId(10L)
            .userName("pink developer")
            .build())
        .date(BirthdayDate.builder()
            .day(2)
            .month(1)
            .year(2009)
            .build())
        .build();

    assertTrue(birthdayStorage.add(birthday.getUserId(), birthday));

    birthday = Birthday.builder()
        .user(BirthdayUser.builder()
            .userId(10L)
            .userName("angry developer")
            .build())
        .date(BirthdayDate.builder()
            .day(25)
            .month(10)
            .year(2003)
            .build())
        .build();

    assertFalse(birthdayStorage.add(birthday.getUserId(), birthday));
  }

  @Test
  void replace() {

    Birthday birthday = Birthday.builder()
        .user(BirthdayUser.builder()
            .userId(1L)
            .userName("test")
            .build())
        .date(BirthdayDate.builder()
            .day(20)
            .month(12)
            .year(2015)
            .build())
        .build();

    final HashMap<Long, Birthday> linkedHashMap = new LinkedHashMap<>();
    linkedHashMap.put(birthday.getUserId(), birthday);

    final BirthdayStorage birthdayStorage = new BirthdayStorage(linkedHashMap);

    birthday = Birthday.builder()
        .user(BirthdayUser.builder()
            .userId(1L)
            .userName("test user name")
            .build())
        .date(BirthdayDate.builder()
            .day(12)
            .month(10)
            .year(1999)
            .build())
        .build();

    assertTrue(birthdayStorage.replace(birthday.getUserId(), birthday));

    birthday = Birthday.builder()
        .user(BirthdayUser.builder()
            .userId(10L)
            .userName("test user name")
            .build())
        .date(BirthdayDate.builder()
            .day(12)
            .month(10)
            .year(1999)
            .build())
        .build();

    assertFalse(birthdayStorage.replace(birthday.getUserId(), birthday));

  }

  @Test
  void remove() {
    Birthday birthday = Birthday.builder()
        .user(BirthdayUser.builder()
            .userId(1L)
            .userName("test")
            .build())
        .date(BirthdayDate.builder()
            .day(20)
            .month(12)
            .year(2015)
            .build())
        .build();


    final HashMap<Long, Birthday> linkedHashMap = new LinkedHashMap<>();
    linkedHashMap.put(birthday.getUserId(), birthday);

    final BirthdayStorage birthdayStorage = new BirthdayStorage(linkedHashMap);

    assertTrue(birthdayStorage.remove(1L));
    assertFalse(birthdayStorage.remove(1L));
    assertFalse(birthdayStorage.remove(40L));
  }

  @Test
  void getAll() {
    final HashMap<Long, Birthday> linkedHashMap = new LinkedHashMap<>();

    Birthday birthday = Birthday.builder()
        .user(BirthdayUser.builder()
            .userId(1L)
            .userName("user 1")
            .enabled(true)
            .build())
        .date(BirthdayDate.builder()
            .day(20)
            .month(12)
            .year(2015)
            .build())
        .build();

    linkedHashMap.put(birthday.getUserId(), birthday);

    birthday = Birthday.builder()
        .user(BirthdayUser.builder()
            .userId(2L)
            .userName("user 2")
            .enabled(true)
            .build())
        .date(BirthdayDate.builder()
            .day(20)
            .month(12)
            .year(2015)
            .build())
        .build();

    linkedHashMap.put(birthday.getUserId(), birthday);

    birthday = Birthday.builder()
        .user(BirthdayUser.builder()
            .userId(3L)
            .userName("user 3")
            .enabled(true)
            .build())
        .date(BirthdayDate.builder()
            .day(20)
            .month(12)
            .year(2015)
            .build())
        .build();

    linkedHashMap.put(birthday.getUserId(), birthday);

    final BirthdayStorage birthdayStorage = new BirthdayStorage(linkedHashMap);

    List<Birthday> birthdays = birthdayStorage.getAll();
    assertNotNull(birthdays);
    assertEquals(3, birthdays.size());
    assertEquals(6L, birthdays.stream().map(Birthday::getUserId).reduce(0L, Long::sum));
  }

  @Test
  void getBirthdayUserByBirthdayDate() {
    final HashMap<Long, Birthday> linkedHashMap = new LinkedHashMap<>();

    Birthday birthday = Birthday.builder()
        .user(BirthdayUser.builder()
            .userId(1L)
            .userName("user 1")
            .enabled(true)
            .build())
        .date(BirthdayDate.builder()
            .day(20)
            .month(12)
            .year(2015)
            .build())
        .build();

    linkedHashMap.put(birthday.getUserId(), birthday);

    birthday = Birthday.builder()
        .user(BirthdayUser.builder()
            .userId(2L)
            .userName("user 1")
            .enabled(true)
            .build())
        .date(BirthdayDate.builder()
            .day(20)
            .month(5)
            .year(2000)
            .build())
        .build();

    linkedHashMap.put(birthday.getUserId(), birthday);

    birthday = Birthday.builder()
        .user(BirthdayUser.builder()
            .userId(3L)
            .userName("user 3")
            .enabled(true)
            .build())
        .date(BirthdayDate.builder()
            .day(10)
            .month(5)
            .year(2005)
            .build())
        .build();

    linkedHashMap.put(birthday.getUserId(), birthday);

    birthday = Birthday.builder()
        .user(BirthdayUser.builder()
            .userId(4L)
            .userName("user 4")
            .enabled(true)
            .build())
        .date(BirthdayDate.builder()
            .day(1)
            .month(1)
            .year(2005)
            .build())
        .build();

    linkedHashMap.put(birthday.getUserId(), birthday);

    final BirthdayStorage birthdayStorage = new BirthdayStorage(linkedHashMap);

    BirthdayDate inputBirthdayDate = BirthdayDate.builder()
        .build();

    List<Birthday> birthdayUserList = birthdayStorage.getBirthdayByBirthdayDate(inputBirthdayDate);

    assertNotNull(birthdayUserList);
    assertEquals(4, birthdayUserList.size());

    inputBirthdayDate = BirthdayDate.builder()
        .day(1)
        .month(1)
        .year(2005)
        .build();

    birthdayUserList = birthdayStorage.getBirthdayByBirthdayDate(inputBirthdayDate);

    assertNotNull(birthdayUserList);
    assertEquals(1, birthdayUserList.size());
    assertEquals(4L, birthdayUserList.get(0).getUserId());

    inputBirthdayDate = BirthdayDate.builder()
        .day(1)
        .month(1)
        .year(2005)
        .build();

    birthdayUserList = birthdayStorage.getBirthdayByBirthdayDate(inputBirthdayDate);

    assertNotNull(birthdayUserList);
    assertEquals(1, birthdayUserList.size());
    assertEquals(4L, sumUserId(birthdayUserList));

    inputBirthdayDate = BirthdayDate.builder()
        .year(2005)
        .build();

    birthdayUserList = birthdayStorage.getBirthdayByBirthdayDate(inputBirthdayDate);

    assertNotNull(birthdayUserList);
    assertEquals(2, birthdayUserList.size());
    assertEquals(7L, sumUserId(birthdayUserList));

    inputBirthdayDate = BirthdayDate.builder()
        .month(5)
        .build();

    birthdayUserList = birthdayStorage.getBirthdayByBirthdayDate(inputBirthdayDate);

    assertNotNull(birthdayUserList);
    assertEquals(2, birthdayUserList.size());
    assertEquals(5L, sumUserId(birthdayUserList));

    inputBirthdayDate = BirthdayDate.builder()
        .day(20)
        .build();

    birthdayUserList = birthdayStorage.getBirthdayByBirthdayDate(inputBirthdayDate);

    assertNotNull(birthdayUserList);
    assertEquals(2, birthdayUserList.size());
    assertEquals(3L, sumUserId(birthdayUserList));

    inputBirthdayDate = BirthdayDate.builder()
        .day(1)
        .month(1)
        .year(2015)
        .build();

    birthdayUserList = birthdayStorage.getBirthdayByBirthdayDate(inputBirthdayDate);

    assertNotNull(birthdayUserList);
    assertEquals(0, birthdayUserList.size());
  }

  @Test
  void findByIdTest() {
    final HashMap<Long, Birthday> linkedHashMap = new LinkedHashMap<>();

    Birthday birthday = Birthday.builder()
        .user(BirthdayUser.builder()
            .userId(1L)
            .userName("user 1")
            .build())
        .date(BirthdayDate.builder()
            .day(20)
            .month(12)
            .year(2015)
            .build())
        .build();

    linkedHashMap.put(birthday.getUserId(), birthday);

    birthday = Birthday.builder()
        .user(BirthdayUser.builder()
            .userId(2L)
            .userName("user 1")
            .build())
        .date(BirthdayDate.builder()
            .day(20)
            .month(5)
            .year(2000)
            .build())
        .build();

    linkedHashMap.put(birthday.getUserId(), birthday);

    birthday = Birthday.builder()
        .user(BirthdayUser.builder()
            .userId(3L)
            .userName("user 3")
            .build())
        .date(BirthdayDate.builder()
            .day(10)
            .month(5)
            .year(2005)
            .build())
        .build();

    linkedHashMap.put(birthday.getUserId(), birthday);

    birthday = Birthday.builder()
        .user(BirthdayUser.builder()
            .userId(4L)
            .userName("user 4")
            .build())
        .date(BirthdayDate.builder()
            .day(1)
            .month(1)
            .year(2005)
            .build())
        .build();

    linkedHashMap.put(birthday.getUserId(), birthday);

    final BirthdayStorage birthdayStorage = new BirthdayStorage(linkedHashMap);
    Optional<Birthday> birthdayQueryResult = birthdayStorage.findById(4L);

    assertTrue(birthdayQueryResult.isPresent());
    assertEquals(birthday, birthdayQueryResult.get());

    birthdayQueryResult = birthdayStorage.findById(40L);

    assertTrue(birthdayQueryResult.isEmpty());


  }


  private long sumUserId(List<Birthday> birthdayUserList) {
    return birthdayUserList.stream().map(Birthday::getUserId).reduce(0L, Long::sum);
  }
}
