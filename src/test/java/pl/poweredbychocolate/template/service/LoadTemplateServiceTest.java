package pl.poweredbychocolate.template.service;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.poweredbychocolate.config.enums.TemplateName;
import pl.poweredbychocolate.storage.StorageService;
import pl.poweredbychocolate.template.domain.Templates;
import pl.poweredbychocolate.template.enums.TemplateType;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class LoadTemplateServiceTest {

  @Mock
  private StorageService storageService;

  @InjectMocks
  private LoadTemplateService loadTemplateService;

  @Test
  void loadTemplates() throws IOException {
    when(storageService.readUserTemplate(TemplateName.SINGLE_USER_TEMPLATE.path())).thenReturn("Single user test template");
    when(storageService.readUserTemplate(TemplateName.MULTI_USER_TEMPLATE.path())).thenReturn("Multi users test template");

    Templates outputTemplates = loadTemplateService.loadTemplates();

    assertNotNull(outputTemplates);
    assertEquals(TemplateType.SINGLE, outputTemplates.getSingleTemplate().getType());
    assertEquals(TemplateType.MULTIPLE, outputTemplates.getMultipleTemplate().getType());
    assertEquals("Single user test template", outputTemplates.getSingleTemplate().getContent());
    assertEquals("Multi users test template", outputTemplates.getMultipleTemplate().getContent());

    verify(storageService).readUserTemplate(TemplateName.SINGLE_USER_TEMPLATE.path());
    verify(storageService).readUserTemplate(TemplateName.MULTI_USER_TEMPLATE.path());
  }
}