package pl.poweredbychocolate.template.service;

import org.junit.jupiter.api.Test;
import pl.poweredbychocolate.birthday.domain.Birthday;
import pl.poweredbychocolate.birthday.domain.BirthdayDate;
import pl.poweredbychocolate.birthday.domain.BirthdayUser;
import pl.poweredbychocolate.template.domain.Template;
import pl.poweredbychocolate.template.enums.TemplateType;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class ParseTemplateServiceTest {


  @Test
  void parseTemplate() {
    final ParseTemplateService parseTemplateService = new ParseTemplateService();

    Birthday inputBirthday = Birthday.builder()
        .user(BirthdayUser.builder()
            .userId(10L)
            .userName("Fenix")
            .build())
        .date(BirthdayDate.builder()
            .month(11)
            .build())
        .build();

    Template inputTemplate = Template.builder()
        .type(TemplateType.SINGLE)
        .content("Today ${user.name} celebrates his birthday. Happy Birthday!!")
        .build();

    String exceptedContent = "Today <@10> celebrates his birthday. Happy Birthday!!";
    assertEquals(exceptedContent, parseTemplateService.parseTemplate(inputTemplate, inputBirthday));

    inputTemplate = Template.builder()
        .type(TemplateType.SINGLE)
        .content("Today ${user.name} celebrates${if.age} his ${user.age} birthday${if.age}. Happy Birthday!!")
        .build();

    exceptedContent = "Today <@10> celebrates. Happy Birthday!!";
    assertEquals(exceptedContent, parseTemplateService.parseTemplate(inputTemplate, inputBirthday));

    inputTemplate = Template.builder()
        .type(TemplateType.SINGLE)
        .content("Today ${user.name} celebrates${if.age} his ${user.age} birthday ${if.age}." +
            "Today ${user.name} celebrates${if.age} his ${user.age} birthday${if.age}." +
            "Today ${user.name} celebrates${if.age} his ${user.age} birthday${if.age}." +
            "Today ${user.name} celebrates${if.age} his ${user.age} birthday${if.age}." +
            "Today ${user.name} celebrates${if.age} his ${user.age} birthday${if.age}.")
        .build();

    exceptedContent = "Today <@10> celebrates." +
        "Today <@10> celebrates." +
        "Today <@10> celebrates." +
        "Today <@10> celebrates." +
        "Today <@10> celebrates.";
    assertEquals(exceptedContent, parseTemplateService.parseTemplate(inputTemplate, inputBirthday));

    inputBirthday = Birthday.builder()
        .user(BirthdayUser.builder()
            .userId(10L)
            .userName("Fenix")
            .build())
        .date(BirthdayDate.builder()
            .month(11)
            .year(2000)
            .build())
        .build();

    int userAge = LocalDate.now().getYear() - inputBirthday.getYear();

    inputTemplate = Template.builder()
        .type(TemplateType.SINGLE)
        .content("Today ${user.name} celebrates${if.age} his ${user.age} birthday${if.age}. Happy Birthday!!")
        .build();

    exceptedContent = "Today <@10> celebrates his " + userAge + " birthday. Happy Birthday!!";
    assertEquals(exceptedContent, parseTemplateService.parseTemplate(inputTemplate, inputBirthday));

    inputTemplate = Template.builder()
        .type(TemplateType.SINGLE)
        .content("Today ${user.name} celebrates ${if.age} his ${user.age} birthday ${if.age}." +
            "Today ${user.name} celebrates ${if.age} his ${user.age} birthday ${if.age}." +
            "Today ${user.name} celebrates ${if.age} his ${user.age} birthday ${if.age}.")
        .build();

    exceptedContent = "Today <@10> celebrates his " + userAge + " birthday." +
        "Today <@10> celebrates his " + userAge + " birthday." +
        "Today <@10> celebrates his " + userAge + " birthday.";
    assertEquals(exceptedContent, parseTemplateService.parseTemplate(inputTemplate, inputBirthday));

    inputTemplate = Template.builder()
        .type(TemplateType.SINGLE)
        .content("Today ${user.name} celebrates ${if.age} his ${user.age} birthday ${if.age}")
        .build();

    exceptedContent = "Today <@10> celebrates his " + userAge + " birthday";
    assertEquals(exceptedContent, parseTemplateService.parseTemplate(inputTemplate, inputBirthday));

  }

  @Test
  void testParseTemplate() {
    final ParseTemplateService parseTemplateService = new ParseTemplateService();
    List<Birthday> birthdayList = new ArrayList<>();

    Template template = Template.builder()
        .type(TemplateType.MULTIPLE)
        .content("Today ${loop} ${user.name} celebrates his ${if.age} ${user.age} ${if.age} birthday. ${loop} Happy Birthday for all!!")
        .build();
    String exceptedContent = "";
    assertEquals(exceptedContent, parseTemplateService.parseTemplate(template, birthdayList));

    birthdayList.add(Birthday.builder()
        .user(BirthdayUser.builder()
            .userId(10L)
            .userName("Fenix")
            .build())
        .date(BirthdayDate.builder()
            .month(11)
            .build())
        .build());

    birthdayList.add(Birthday.builder()
        .user(BirthdayUser.builder()
            .userId(20L)
            .userName("Angel")
            .build())
        .date(BirthdayDate.builder()
            .month(11)
            .day(10)
            .year(2000)
            .build())
        .build());

    int userAge = LocalDate.now().getYear() - 2000;
    exceptedContent = "Today <@10> celebrates his birthday. " +
        "<@20> celebrates his "+userAge+" birthday. Happy Birthday for all!!";
    assertEquals(exceptedContent, parseTemplateService.parseTemplate(template, birthdayList));


    template = Template.builder()
        .type(TemplateType.MULTIPLE)
        .content("Today ${user.name} celebrates his ${user.age} birthday.")
        .build();
    exceptedContent = "Today ${user.name} celebrates his ${user.age} birthday.";
    assertEquals(exceptedContent, parseTemplateService.parseTemplate(template, birthdayList));
  }
}