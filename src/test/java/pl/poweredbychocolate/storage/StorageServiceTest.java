package pl.poweredbychocolate.storage;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.poweredbychocolate.storage.domain.ConfigFile;
import pl.poweredbychocolate.storage.repository.StorageRepository;

import java.io.IOException;
import java.nio.file.Path;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class StorageServiceTest {

  @Mock
  private StorageRepository storageRepository;

  @InjectMocks
  private StorageService storageService;

  @Test
  void saveConfigFileTest() throws IOException {
    ConfigFile configFile = ConfigFile.builder()
        .notificationChannelId(123456789L)
        .botToken("test-token")
        .build();

    doNothing().when(storageRepository).saveYamlToFile(any(ConfigFile.class), any(Path.class));
    assertDoesNotThrow(() -> storageService.saveConfigFile(configFile));

    doThrow(new IOException()).when(storageRepository).saveYamlToFile(any(ConfigFile.class), any(Path.class));
    assertThrows(IOException.class, () -> storageService.saveConfigFile(configFile));

    verify(storageRepository, times(2)).saveYamlToFile(any(ConfigFile.class), any(Path.class));

  }

  @Test
  void saveUserTemplateTest() throws IOException {

    doNothing().when(storageRepository).saveTextToFile(anyString(), any(Path.class));
    assertDoesNotThrow(() -> storageService.saveUserTemplate("User Template", Path.of("template1.txt")));

    doThrow(new IOException()).when(storageRepository).saveTextToFile(anyString(), any(Path.class));
    assertThrows(IOException.class, () -> storageService.saveUserTemplate("User Template", Path.of("template1.txt")));

    verify(storageRepository, times(2)).saveTextToFile(anyString(), any(Path.class));

  }

  @Test
  void readConfigFileTest() throws IOException {
    ConfigFile patternConfigFile = ConfigFile.builder()
        .notificationChannelId(123456789L)
        .botToken("test-token")
        .build();

    when(storageRepository.readYAMLFromFile(any(Path.class), any())).thenReturn(patternConfigFile);
    assertDoesNotThrow(() -> {
      ConfigFile responseConfigFile = storageService.readConfigFile();
      assertNotNull(responseConfigFile);
      assertEquals(patternConfigFile.getBotToken(), responseConfigFile.getBotToken());
      assertEquals(patternConfigFile.getNotificationChannelId(), responseConfigFile.getNotificationChannelId());
    });

    doThrow(new IOException()).when(storageRepository).readYAMLFromFile(any(Path.class), any());
    assertThrows(IOException.class, () -> storageService.readConfigFile());

    verify(storageRepository, times(2)).readYAMLFromFile(any(Path.class), any());
  }

  @Test
  void readUserTemplate() throws IOException {
    when(storageRepository.readTextFromFile(any(Path.class))).thenReturn("Simple template");

    Path testPath = Path.of("test");
    assertDoesNotThrow(() -> {
      String queryResult = storageService.readUserTemplate(testPath);
      assertEquals("Simple template", queryResult);
    });

    doThrow(new IOException()).when(storageRepository).readTextFromFile(any(Path.class));

    assertThrows(IOException.class, () -> storageService.readUserTemplate(testPath));

    verify(storageRepository, times(2)).readTextFromFile(any(Path.class));
  }
}