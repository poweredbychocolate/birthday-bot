package pl.poweredbychocolate.storage.repository;

import org.junit.jupiter.api.Test;
import pl.poweredbychocolate.storage.domain.ConfigFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import static org.junit.jupiter.api.Assertions.*;

class StorageRepositoryTest {
  private final StorageRepository storageRepository = new StorageRepository();

  @Test
  void isFileExistTest() {
    assertTrue(storageRepository.isFileExist(Path.of("src/test/resources/storage/birthday.txt")));
    assertFalse(storageRepository.isFileExist(Path.of("src/test/resources/storage/birthday.yml")));
  }

  @Test
  void saveTextToFileTest() {
    assertDoesNotThrow(() -> {
      String textToSave = "Today is saveTextToFile birthday";
      Path pathToSave = Path.of("src/test/resources/storage/save-to-file.txt");
      storageRepository.saveTextToFile(textToSave, pathToSave);

      assertTrue(Files.exists(pathToSave));
      Files.deleteIfExists(pathToSave);
    });

    assertThrows(IOException.class, () -> {
      String textToSave = "Today is saveTextToFile birthday";
      Path pathToSave = Path.of("/none/storage/save-to-file.txt");
      storageRepository.saveTextToFile(textToSave, pathToSave);
    });
  }

  @Test
  void readTextFromFileTest() {
    assertDoesNotThrow(() -> {
      String exceptedText = "Today is readTextFromFile birthday";
      Path pathToRead = Path.of("src/test/resources/storage/read-from-file.txt");
      String textFromFile = storageRepository.readTextFromFile(pathToRead);

      assertEquals(exceptedText, textFromFile);
    });
  }

  @Test
  void saveYamlToFileTest() {
    ConfigFile configFile = ConfigFile.builder()
        .botToken("bot-token-here")
        .notificationChannelId(123678L)
        .build();

    assertDoesNotThrow(() -> {
      Path pathToSave = Path.of("src/test/resources/storage/save-from-yaml-file.yml");
      storageRepository.saveYamlToFile(configFile, pathToSave);

      assertTrue(Files.exists(pathToSave));
      Files.deleteIfExists(pathToSave);
    });

    assertThrows(IOException.class, () -> {
      Path pathToSave = Path.of("/none/storage/save-from-yaml-file.yml");
      storageRepository.saveYamlToFile(configFile, pathToSave);
    });
  }

  @Test
  void readYAMLFromFileTest() {
    assertDoesNotThrow(() -> {
      Path pathToRead = Path.of("src/test/resources/storage/read-from-yaml-file.yml");
      ConfigFile configFile = storageRepository.readYAMLFromFile(pathToRead, ConfigFile.class);

      assertNotNull(configFile);
      assertEquals("bot-token-here", configFile.getBotToken());
      assertEquals(123678L, configFile.getNotificationChannelId());
    });
  }

}
