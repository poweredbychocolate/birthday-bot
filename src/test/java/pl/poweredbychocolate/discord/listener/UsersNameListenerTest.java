package pl.poweredbychocolate.discord.listener;

import org.javacord.api.entity.user.User;
import org.javacord.api.event.user.UserChangeNameEvent;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.poweredbychocolate.birthday.domain.Birthday;
import pl.poweredbychocolate.birthday.domain.BirthdayDate;
import pl.poweredbychocolate.birthday.domain.BirthdayUser;
import pl.poweredbychocolate.birthday.repository.BirthdayRepositoryPort;

import java.io.IOException;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class UsersNameListenerTest {

  @Mock
  private BirthdayRepositoryPort birthdayRepositoryPort;

  @InjectMocks
  private UsersNameListener usersNameListener;

  @Test
  void onUserChangeNameNoUserTest() throws IOException {
    assertNotNull(usersNameListener);

    UserChangeNameEvent userChangeNameEvent = mock(UserChangeNameEvent.class);
    User userMock = mock(User.class);

    when(userChangeNameEvent.getUser()).thenReturn(userMock);
    when(userMock.getName()).thenReturn("Mock user name");
    when(userMock.getId()).thenReturn(10L);

    when(birthdayRepositoryPort.findById(10L)).thenReturn(Optional.empty());

    usersNameListener.onUserChangeName(userChangeNameEvent);

    verify(birthdayRepositoryPort).findById(10L);
    verify(birthdayRepositoryPort, never()).replace(any());
  }

  @Test
  void onUserChangeNameUserExistTest() throws IOException {

    assertNotNull(usersNameListener);

    UserChangeNameEvent userChangeNameEvent = mock(UserChangeNameEvent.class);
    User userMock = mock(User.class);

    when(userChangeNameEvent.getUser()).thenReturn(userMock);
    when(userMock.getName()).thenReturn("Mock user name");
    when(userMock.getId()).thenReturn(20L);

    Birthday birthday = Birthday.builder()
        .user(BirthdayUser.builder()
            .userId(20L)
            .build())
        .date(BirthdayDate.builder()
            .build())
        .build();

    when(birthdayRepositoryPort.findById(20L)).thenReturn(Optional.of(birthday));

    usersNameListener.onUserChangeName(userChangeNameEvent);

    when(birthdayRepositoryPort.replace(any(Birthday.class))).thenThrow(new IOException());
    usersNameListener.onUserChangeName(userChangeNameEvent);

    verify(birthdayRepositoryPort, times(2)).findById(20L);
    verify(birthdayRepositoryPort, times(2)).replace(any());
  }
}