package pl.poweredbychocolate.discord.listener;

import org.javacord.api.entity.channel.TextChannel;
import org.javacord.api.entity.message.MessageAuthor;
import org.javacord.api.entity.message.MessageBuilder;
import org.javacord.api.entity.server.Server;
import org.javacord.api.event.message.MessageCreateEvent;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.poweredbychocolate.auto.task.TimerTaskScheduler;
import pl.poweredbychocolate.birthday.domain.Birthday;
import pl.poweredbychocolate.birthday.domain.BirthdayDate;
import pl.poweredbychocolate.birthday.domain.BirthdayUser;
import pl.poweredbychocolate.birthday.repository.BirthdayRepositoryPort;
import pl.poweredbychocolate.calendar.service.CalendarBuildService;
import pl.poweredbychocolate.command.domain.BirthdayAddCommand;
import pl.poweredbychocolate.command.domain.BirthdayRemoveCommand;
import pl.poweredbychocolate.command.domain.BirthdayWhoCommand;
import pl.poweredbychocolate.command.enums.AdminCommandHeaderType;
import pl.poweredbychocolate.command.enums.CommandHeaderType;
import pl.poweredbychocolate.command.service.AdminCommandService;
import pl.poweredbychocolate.command.service.CommandService;
import pl.poweredbychocolate.discord.service.CommandStateResponseService;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.mockito.Mockito.*;


@ExtendWith(MockitoExtension.class)
class BotCommandListenerTest {
  @Mock
  private MessageCreateEvent messageCreateEventMock;
  @Mock
  private CommandService commandServiceMock;
  @Mock
  private BirthdayRepositoryPort birthdayRepositoryPortMock;
  @Mock
  private CommandStateResponseService commandStateResponseServiceMock;
  @Mock
  private MessageAuthor messageAuthorMock;
  @Mock
  private TextChannel textChannelMock;
  @Mock
  private MessageBuilder messageBuilderMock;
  @Mock
  private AdminCommandService adminCommandServiceMock;
  @Mock
  private TimerTaskScheduler timerTaskScheduler;
  @Mock
  private Server serverMock;
  @Mock
  private CalendarBuildService calendarBuildServiceMock;
  @Mock
  private ResourceBundle userResourceBundleMock;
  @InjectMocks
  private BotCommandListener botCommandListener;


  @Test
  void onMessageCreateNoUserMessageTest() {
    when(messageCreateEventMock.getMessageAuthor()).thenReturn(messageAuthorMock);
    when(messageCreateEventMock.isServerMessage()).thenReturn(true);
    when(messageAuthorMock.isRegularUser()).thenReturn(false);
    when(messageAuthorMock.isServerAdmin()).thenReturn(false);

    botCommandListener.onMessageCreate(messageCreateEventMock);

    verifyNoInteractions(commandServiceMock, birthdayRepositoryPortMock, commandStateResponseServiceMock, adminCommandServiceMock, timerTaskScheduler);
  }

  @Test
  void onMessageCreateNoMatchCommandTest() {
    when(messageCreateEventMock.getMessageAuthor()).thenReturn(messageAuthorMock);
    when(messageCreateEventMock.isServerMessage()).thenReturn(true);
    when(messageAuthorMock.isRegularUser()).thenReturn(true);
    when(messageCreateEventMock.getMessageContent()).thenReturn("$unknown command");

    when(messageCreateEventMock.getServer()).thenReturn(Optional.of(serverMock));
    when(commandServiceMock.parseCommandType("$unknown command")).thenReturn(Optional.empty());

    botCommandListener.onMessageCreate(messageCreateEventMock);

    verifyNoInteractions(birthdayRepositoryPortMock, commandStateResponseServiceMock);
    verify(commandServiceMock).parseCommandType(anyString());
    verifyNoInteractions(adminCommandServiceMock, timerTaskScheduler);
  }

  @Test
  void onMessageCreateAddCommandNotParsedBirthdayTest() {
    when(messageCreateEventMock.getMessageAuthor()).thenReturn(messageAuthorMock);
    when(messageCreateEventMock.isServerMessage()).thenReturn(true);
    when(messageCreateEventMock.getChannel()).thenReturn(textChannelMock);
    when(messageAuthorMock.isRegularUser()).thenReturn(true);
    when(messageAuthorMock.getId()).thenReturn(1000L);
    when(messageAuthorMock.getName()).thenReturn("TEST");
    when(commandStateResponseServiceMock.commandIncorrectDate())
        .thenReturn(messageBuilderMock);

    when(messageCreateEventMock.getMessageContent()).thenReturn("$birthday add");
    when(commandServiceMock.parseCommandType("$birthday add")).thenReturn(Optional.of(CommandHeaderType.USER_ADD));

    when(messageCreateEventMock.getServer()).thenReturn(Optional.of(serverMock));
    when(commandServiceMock.parseUserAddCommand(eq("$birthday add"), anyLong(), anyString()))
        .thenReturn(Optional.empty());

    botCommandListener.onMessageCreate(messageCreateEventMock);

    verifyNoInteractions(birthdayRepositoryPortMock);
    verify(commandServiceMock).parseCommandType(anyString());
    verify(commandServiceMock).parseUserAddCommand(eq("$birthday add"), anyLong(), anyString());
    verify(commandStateResponseServiceMock).commandIncorrectDate();
    verifyNoInteractions(adminCommandServiceMock, timerTaskScheduler);
  }

  @Test
  void onMessageCreateAddCommandSaveAndReplaceBirthdayTest() throws IOException {
    when(messageCreateEventMock.getMessageAuthor()).thenReturn(messageAuthorMock);
    when(messageCreateEventMock.isServerMessage()).thenReturn(true);
    when(messageCreateEventMock.getChannel()).thenReturn(textChannelMock);
    when(messageAuthorMock.isRegularUser()).thenReturn(true);
    when(messageAuthorMock.getId()).thenReturn(1000L);
    when(messageAuthorMock.getName()).thenReturn("TEST");
    when(commandStateResponseServiceMock.commandAddSaved())
        .thenReturn(messageBuilderMock);
    when(commandStateResponseServiceMock.commandAddReplaced())
        .thenReturn(messageBuilderMock);

    when(messageCreateEventMock.getMessageContent()).thenReturn("$birthday add");
    when(commandServiceMock.parseCommandType("$birthday add")).thenReturn(Optional.of(CommandHeaderType.USER_ADD));

    when(messageCreateEventMock.getServer()).thenReturn(Optional.of(serverMock));
    when(commandServiceMock.parseUserAddCommand(eq("$birthday add"), anyLong(), anyString()))
        .thenReturn(Optional.of(BirthdayAddCommand.builder()
            .userId(1L)
            .userName("Test user 1")
            .year(2000)
            .month(2)
            .day(1)
            .build()));

    when(birthdayRepositoryPortMock.findById(anyLong())).thenReturn(Optional.empty())
        .thenReturn(Optional.of(Birthday.builder()
            .user(BirthdayUser.builder()
                .userName("Test user")
                .userId(1L)
                .build())
            .date(BirthdayDate.builder()
                .year(2000)
                .month(3)
                .day(20)
                .build())
            .build()));


    botCommandListener.onMessageCreate(messageCreateEventMock);
    botCommandListener.onMessageCreate(messageCreateEventMock);

    verify(commandServiceMock, times(2)).parseCommandType(anyString());
    verify(commandServiceMock, times(2)).parseUserAddCommand(eq("$birthday add"), anyLong(), anyString());
    verify(commandStateResponseServiceMock).commandAddSaved();
    verify(commandStateResponseServiceMock).commandAddReplaced();
    verify(birthdayRepositoryPortMock, times(2)).findById(anyLong());
    verify(birthdayRepositoryPortMock).save(any(Birthday.class));
    verify(birthdayRepositoryPortMock).replace(any(Birthday.class));
    verify(commandStateResponseServiceMock, never()).unexpectedError();
    verify(commandStateResponseServiceMock, never()).commandIncorrectDate();
    verifyNoInteractions(adminCommandServiceMock, timerTaskScheduler);
  }

  @Test
  void onMessageCreateAddCommandThrowExceptionBirthdayTest() throws IOException {
    when(messageCreateEventMock.getMessageAuthor()).thenReturn(messageAuthorMock);
    when(messageCreateEventMock.isServerMessage()).thenReturn(true);
    when(messageCreateEventMock.getChannel()).thenReturn(textChannelMock);
    when(messageAuthorMock.isRegularUser()).thenReturn(true);
    when(messageAuthorMock.getId()).thenReturn(1000L);
    when(messageAuthorMock.getName()).thenReturn("TEST");
    when(commandStateResponseServiceMock.unexpectedError())
        .thenReturn(messageBuilderMock);

    when(messageCreateEventMock.getMessageContent()).thenReturn("$birthday add");
    when(commandServiceMock.parseCommandType("$birthday add")).thenReturn(Optional.of(CommandHeaderType.USER_ADD));

    when(commandServiceMock.parseUserAddCommand(eq("$birthday add"), anyLong(), anyString()))
        .thenReturn(Optional.of(BirthdayAddCommand.builder()
            .userId(1L)
            .userName("Test user 1")
            .year(2000)
            .month(2)
            .day(1)
            .build()));

    when(birthdayRepositoryPortMock.findById(anyLong())).thenReturn(Optional.empty())
        .thenReturn(Optional.of(Birthday.builder()
            .user(BirthdayUser.builder()
                .userName("Test user")
                .userId(1L)
                .build())
            .date(BirthdayDate.builder()
                .year(2000)
                .month(3)
                .day(20)
                .build())
            .build()));

    when(birthdayRepositoryPortMock.save(any(Birthday.class))).thenThrow(new IOException());
    when(birthdayRepositoryPortMock.replace(any(Birthday.class))).thenThrow(new IOException());
    when(messageCreateEventMock.getServer()).thenReturn(Optional.of(serverMock));

    assertDoesNotThrow(() -> botCommandListener.onMessageCreate(messageCreateEventMock));
    assertDoesNotThrow(() -> botCommandListener.onMessageCreate(messageCreateEventMock));

    verify(commandServiceMock, times(2)).parseCommandType(anyString());
    verify(commandServiceMock, times(2)).parseUserAddCommand(eq("$birthday add"), anyLong(), anyString());
    verify(commandStateResponseServiceMock, never()).commandAddSaved();
    verify(commandStateResponseServiceMock, never()).commandAddReplaced();
    verify(birthdayRepositoryPortMock, times(2)).findById(anyLong());
    verify(birthdayRepositoryPortMock).save(any(Birthday.class));
    verify(birthdayRepositoryPortMock).replace(any(Birthday.class));
    verify(commandStateResponseServiceMock, times(2)).unexpectedError();
    verify(commandStateResponseServiceMock, never()).commandIncorrectDate();
    verifyNoInteractions(adminCommandServiceMock, timerTaskScheduler);
  }

  @Test
  void onMessageCreateRemoveCommandNotParsedBirthdayTest() throws IOException {
    when(messageCreateEventMock.getMessageAuthor()).thenReturn(messageAuthorMock);
    when(messageCreateEventMock.isServerMessage()).thenReturn(true);
    when(messageCreateEventMock.getChannel()).thenReturn(textChannelMock);
    when(messageAuthorMock.isRegularUser()).thenReturn(true);
    when(messageAuthorMock.getId()).thenReturn(1000L);
    when(messageAuthorMock.getName()).thenReturn("TEST");
    when(commandStateResponseServiceMock.commandRemoveNoUser())
        .thenReturn(messageBuilderMock);

    when(messageCreateEventMock.getMessageContent()).thenReturn("$birthday remove");
    when(commandServiceMock.parseCommandType("$birthday remove")).thenReturn(Optional.of(CommandHeaderType.USER_REMOVE));

    when(birthdayRepositoryPortMock.findById(anyLong())).thenReturn(Optional.empty());
    when(commandServiceMock.parseUserRemoveCommand(anyLong(), anyString()))
        .thenReturn(BirthdayRemoveCommand.builder()
            .userId(1000L)
            .userName("TEST")
            .build());
    when(messageCreateEventMock.getServer()).thenReturn(Optional.of(serverMock));
    botCommandListener.onMessageCreate(messageCreateEventMock);

    verify(commandServiceMock).parseCommandType(anyString());
    verify(commandServiceMock).parseUserRemoveCommand(anyLong(), anyString());
    verify(commandStateResponseServiceMock).commandRemoveNoUser();

    verify(birthdayRepositoryPortMock, never()).delete(any(BirthdayUser.class));
    verify(commandStateResponseServiceMock, never()).unexpectedError();
    verify(commandStateResponseServiceMock, never()).commandRemoveSuccessfully();
    verifyNoInteractions(adminCommandServiceMock, timerTaskScheduler);
  }

  @Test
  void onMessageCreateRemoveCommandDeleteBirthdayTest() throws IOException {
    when(messageCreateEventMock.getMessageAuthor()).thenReturn(messageAuthorMock);
    when(messageCreateEventMock.isServerMessage()).thenReturn(true);
    when(messageCreateEventMock.getChannel()).thenReturn(textChannelMock);
    when(messageAuthorMock.isRegularUser()).thenReturn(true);
    when(messageAuthorMock.getId()).thenReturn(1000L);
    when(messageAuthorMock.getName()).thenReturn("TEST");
    when(commandStateResponseServiceMock.commandRemoveSuccessfully())
        .thenReturn(messageBuilderMock);

    when(messageCreateEventMock.getServer()).thenReturn(Optional.of(serverMock));
    when(messageCreateEventMock.getMessageContent()).thenReturn("$birthday remove");
    when(commandServiceMock.parseCommandType("$birthday remove")).thenReturn(Optional.of(CommandHeaderType.USER_REMOVE));

    when(birthdayRepositoryPortMock.findById(anyLong()))
        .thenReturn(Optional.of(Birthday.builder()
            .user(BirthdayUser.builder()
                .userId(1000L)
                .userName("TEST")
                .build())
            .date(BirthdayDate.builder()
                .day(1)
                .month(10)
                .year(2007)
                .build())
            .build()));

    when(commandServiceMock.parseUserRemoveCommand(anyLong(), anyString()))
        .thenReturn(BirthdayRemoveCommand.builder()
            .userId(1000L)
            .userName("TEST")
            .build());

    botCommandListener.onMessageCreate(messageCreateEventMock);

    verify(commandServiceMock).parseCommandType(anyString());
    verify(commandServiceMock).parseUserRemoveCommand(anyLong(), anyString());
    verify(commandStateResponseServiceMock).commandRemoveSuccessfully();
    verify(birthdayRepositoryPortMock).delete(any(BirthdayUser.class));

    verify(commandStateResponseServiceMock, never()).unexpectedError();
    verify(commandStateResponseServiceMock, never()).commandRemoveNoUser();
    verifyNoInteractions(adminCommandServiceMock, timerTaskScheduler);
  }

  @Test
  void onMessageCreateRemoveCommandThrowExceptionBirthdayTest() throws IOException {
    when(messageCreateEventMock.getMessageAuthor()).thenReturn(messageAuthorMock);
    when(messageCreateEventMock.isServerMessage()).thenReturn(true);
    when(messageCreateEventMock.getChannel()).thenReturn(textChannelMock);
    when(messageAuthorMock.isRegularUser()).thenReturn(true);
    when(messageAuthorMock.getId()).thenReturn(1000L);
    when(messageAuthorMock.getName()).thenReturn("TEST");
    when(commandStateResponseServiceMock.unexpectedError())
        .thenReturn(messageBuilderMock);

    when(messageCreateEventMock.getMessageContent()).thenReturn("$birthday remove");
    when(commandServiceMock.parseCommandType("$birthday remove")).thenReturn(Optional.of(CommandHeaderType.USER_REMOVE));

    when(birthdayRepositoryPortMock.findById(anyLong()))
        .thenReturn(Optional.of(Birthday.builder()
            .user(BirthdayUser.builder()
                .userId(1000L)
                .userName("TEST")
                .build())
            .date(BirthdayDate.builder()
                .day(1)
                .month(10)
                .year(2007)
                .build())
            .build()));

    when(commandServiceMock.parseUserRemoveCommand(anyLong(), anyString()))
        .thenReturn(BirthdayRemoveCommand.builder()
            .userId(1000L)
            .userName("TEST")
            .build());

    when(birthdayRepositoryPortMock.delete(any(BirthdayUser.class))).thenThrow(new IOException());
    when(messageCreateEventMock.getServer()).thenReturn(Optional.of(serverMock));

    assertDoesNotThrow(() -> botCommandListener.onMessageCreate(messageCreateEventMock));

    verify(commandServiceMock).parseCommandType(anyString());
    verify(commandServiceMock).parseUserRemoveCommand(anyLong(), anyString());
    verify(commandStateResponseServiceMock).unexpectedError();
    verify(birthdayRepositoryPortMock).delete(any(BirthdayUser.class));

    verify(commandStateResponseServiceMock, never()).commandRemoveSuccessfully();
    verify(commandStateResponseServiceMock, never()).commandRemoveNoUser();
    verifyNoInteractions(adminCommandServiceMock, timerTaskScheduler);
  }

  @Test
  void onMessageCreateWhoCommandNotParsedBirthdayTest() {
    when(messageCreateEventMock.getMessageAuthor()).thenReturn(messageAuthorMock);
    when(messageCreateEventMock.isServerMessage()).thenReturn(true);
    when(messageCreateEventMock.getChannel()).thenReturn(textChannelMock);
    when(messageAuthorMock.isRegularUser()).thenReturn(true);
    when(commandStateResponseServiceMock.commandIncorrectDate())
        .thenReturn(messageBuilderMock);

    when(messageCreateEventMock.getServer()).thenReturn(Optional.of(serverMock));
    when(messageCreateEventMock.getMessageContent()).thenReturn("$birthday who");
    when(commandServiceMock.parseCommandType("$birthday who")).thenReturn(Optional.of(CommandHeaderType.USER_WHO));

    when(commandServiceMock.parseUserWhoCommand("$birthday who"))
        .thenReturn(Optional.empty());

    botCommandListener.onMessageCreate(messageCreateEventMock);

    verify(commandServiceMock).parseCommandType(anyString());
    verify(commandServiceMock).parseUserWhoCommand(anyString());
    verify(commandStateResponseServiceMock).commandIncorrectDate();

    verifyNoInteractions(birthdayRepositoryPortMock);
    verify(commandStateResponseServiceMock, never()).whoCommandEmptyUsersList();
    verify(commandStateResponseServiceMock, never()).whoCommandUsersListResponse(anyList());
    verifyNoInteractions(adminCommandServiceMock, timerTaskScheduler);
  }

  @Test
  void onMessageCreateWhoCommandEmptyUserListBirthdayTest() {
    when(messageCreateEventMock.getMessageAuthor()).thenReturn(messageAuthorMock);
    when(messageCreateEventMock.isServerMessage()).thenReturn(true);
    when(messageCreateEventMock.getChannel()).thenReturn(textChannelMock);
    when(messageAuthorMock.isRegularUser()).thenReturn(true);
    when(commandStateResponseServiceMock.whoCommandEmptyUsersList())
        .thenReturn(messageBuilderMock);

    when(messageCreateEventMock.getMessageContent()).thenReturn("$birthday who");
    when(commandServiceMock.parseCommandType("$birthday who")).thenReturn(Optional.of(CommandHeaderType.USER_WHO));
    when(messageCreateEventMock.getServer()).thenReturn(Optional.of(serverMock));

    when(commandServiceMock.parseUserWhoCommand("$birthday who"))
        .thenReturn(Optional.of(BirthdayWhoCommand.builder()
            .day(5)
            .month(5)
            .year(2005)
            .build()));
    when(birthdayRepositoryPortMock.findBirthdayByBirthdayDateIfEnabled(any(BirthdayDate.class)))
        .thenReturn(Collections.emptyList());

    botCommandListener.onMessageCreate(messageCreateEventMock);

    verify(commandServiceMock).parseCommandType(anyString());
    verify(commandServiceMock).parseUserWhoCommand(anyString());
    verify(commandStateResponseServiceMock).whoCommandEmptyUsersList();

    verify(birthdayRepositoryPortMock).findBirthdayByBirthdayDateIfEnabled(any(BirthdayDate.class));
    verify(commandStateResponseServiceMock, never()).commandIncorrectDate();
    verify(commandStateResponseServiceMock, never()).whoCommandUsersListResponse(anyList());
    verifyNoInteractions(adminCommandServiceMock, timerTaskScheduler);
  }

  @Test
  void onMessageCreateWhoCommandNotEmptyUserListBirthdayTest() {
    when(messageCreateEventMock.getMessageAuthor()).thenReturn(messageAuthorMock);
    when(messageCreateEventMock.isServerMessage()).thenReturn(true);
    when(messageCreateEventMock.getChannel()).thenReturn(textChannelMock);
    when(messageAuthorMock.isRegularUser()).thenReturn(true);
    when(commandStateResponseServiceMock.whoCommandUsersListResponse(anyList()))
        .thenReturn(messageBuilderMock);

    when(messageCreateEventMock.getMessageContent()).thenReturn("$birthday who");
    when(commandServiceMock.parseCommandType("$birthday who")).thenReturn(Optional.of(CommandHeaderType.USER_WHO));

    when(commandServiceMock.parseUserWhoCommand("$birthday who"))
        .thenReturn(Optional.of(BirthdayWhoCommand.builder()
            .day(5)
            .month(5)
            .year(2005)
            .build()));

    when(messageCreateEventMock.getServer()).thenReturn(Optional.of(serverMock));
    when(birthdayRepositoryPortMock.findBirthdayByBirthdayDateIfEnabled(any(BirthdayDate.class)))
        .thenReturn(List.of(Birthday.builder()
                .user(BirthdayUser.builder().userId(1L).userName("TEST1").build())
                .date(BirthdayDate.builder().day(1).month(2).year(2002).build())
                .build(),
            Birthday.builder()
                .user(BirthdayUser.builder().userId(2L).userName("TEST2").build())
                .date(BirthdayDate.builder().day(1).month(2).year(2002).build())
                .build()
        ));

    botCommandListener.onMessageCreate(messageCreateEventMock);

    verify(commandServiceMock).parseCommandType(anyString());
    verify(commandServiceMock).parseUserWhoCommand(anyString());
    verify(commandStateResponseServiceMock).whoCommandUsersListResponse(anyList());

    verify(birthdayRepositoryPortMock).findBirthdayByBirthdayDateIfEnabled(any(BirthdayDate.class));
    verify(commandStateResponseServiceMock, never()).commandIncorrectDate();
    verify(commandStateResponseServiceMock, never()).whoCommandEmptyUsersList();
    verifyNoInteractions(adminCommandServiceMock, timerTaskScheduler);
  }

  @Test
  void handleAdminBirthdayThreadStart() {
    when(messageCreateEventMock.getMessageAuthor()).thenReturn(messageAuthorMock);
    when(messageCreateEventMock.isServerMessage()).thenReturn(false);
    when(messageCreateEventMock.isPrivateMessage()).thenReturn(true);
    when(adminCommandServiceMock.hasPermission(anyLong())).thenReturn(true);
    when(commandStateResponseServiceMock.adminThreadStart())
        .thenReturn(messageBuilderMock);

    when(messageCreateEventMock.getMessageContent()).thenReturn("$thread start");
    when(adminCommandServiceMock.parseCommandType("$thread start")).thenReturn(Optional.of(AdminCommandHeaderType.START_BIRTHDAY_THREAD));

    botCommandListener.onMessageCreate(messageCreateEventMock);

    verify(adminCommandServiceMock).parseCommandType(anyString());
    verify(adminCommandServiceMock).hasPermission(anyLong());
    verify(timerTaskScheduler).runThread();
    verify(commandStateResponseServiceMock).adminThreadStart();

    verifyNoInteractions(commandServiceMock, birthdayRepositoryPortMock);
  }

  @Test
  void onMessageCreateAdminUserRemoveNotParsedBirthdayTest() throws IOException {
    when(messageCreateEventMock.getMessageAuthor()).thenReturn(messageAuthorMock);
    when(messageCreateEventMock.isServerMessage()).thenReturn(false);
    when(messageCreateEventMock.isPrivateMessage()).thenReturn(true);
    when(adminCommandServiceMock.hasPermission(anyLong())).thenReturn(true);

    when(commandStateResponseServiceMock.adminCommandIncorrectUserId())
        .thenReturn(messageBuilderMock);

    when(messageCreateEventMock.getMessageContent()).thenReturn("$admin remove");
    when(adminCommandServiceMock.parseCommandType("$admin remove")).thenReturn(Optional.of(AdminCommandHeaderType.REMOVE_USER));

    botCommandListener.onMessageCreate(messageCreateEventMock);

    verify(adminCommandServiceMock).parseCommandType(anyString());
    verify(commandServiceMock).parseAdminUserRemoveCommand(anyString());
    verify(commandStateResponseServiceMock).adminCommandIncorrectUserId();

    verify(birthdayRepositoryPortMock, never()).deleteById(anyLong());
    verify(commandStateResponseServiceMock, never()).unexpectedError();
    verify(commandStateResponseServiceMock, never()).adminCommandUserRemoved();
    verifyNoInteractions(timerTaskScheduler);
  }

  @Test
  void onMessageCreateAdminUserRemoveSuccessfullyBirthdayTest() throws IOException {
    when(messageCreateEventMock.getMessageAuthor()).thenReturn(messageAuthorMock);
    when(messageCreateEventMock.isServerMessage()).thenReturn(false);
    when(messageCreateEventMock.isPrivateMessage()).thenReturn(true);
    when(adminCommandServiceMock.hasPermission(anyLong())).thenReturn(true);

    when(commandStateResponseServiceMock.adminCommandUserRemoved())
        .thenReturn(messageBuilderMock);

    when(messageCreateEventMock.getMessageContent()).thenReturn("$admin remove");
    when(adminCommandServiceMock.parseCommandType("$admin remove")).thenReturn(Optional.of(AdminCommandHeaderType.REMOVE_USER));
    when(commandServiceMock.parseAdminUserRemoveCommand(anyString())).thenReturn(Optional.of(1L));

    botCommandListener.onMessageCreate(messageCreateEventMock);

    verify(adminCommandServiceMock).parseCommandType(anyString());
    verify(commandServiceMock).parseAdminUserRemoveCommand(anyString());
    verify(commandStateResponseServiceMock).adminCommandUserRemoved();

    verify(birthdayRepositoryPortMock).deleteById(anyLong());
    verify(commandStateResponseServiceMock, never()).unexpectedError();
    verify(commandStateResponseServiceMock, never()).adminCommandIncorrectUserId();
    verifyNoInteractions(timerTaskScheduler);
  }

  @Test
  void onMessageCreateAdminUserRemoveErrorBirthdayTest() throws IOException {
    when(messageCreateEventMock.getMessageAuthor()).thenReturn(messageAuthorMock);
    when(messageCreateEventMock.isServerMessage()).thenReturn(false);
    when(messageCreateEventMock.isPrivateMessage()).thenReturn(true);
    when(adminCommandServiceMock.hasPermission(anyLong())).thenReturn(true);

    when(commandStateResponseServiceMock.unexpectedError())
        .thenReturn(messageBuilderMock);

    when(messageCreateEventMock.getMessageContent()).thenReturn("$admin remove");
    when(adminCommandServiceMock.parseCommandType("$admin remove")).thenReturn(Optional.of(AdminCommandHeaderType.REMOVE_USER));
    when(commandServiceMock.parseAdminUserRemoveCommand(anyString())).thenReturn(Optional.of(1L));
    when(birthdayRepositoryPortMock.deleteById(anyLong())).thenThrow(new IOException());

    botCommandListener.onMessageCreate(messageCreateEventMock);

    verify(adminCommandServiceMock).parseCommandType(anyString());
    verify(commandServiceMock).parseAdminUserRemoveCommand(anyString());
    verify(commandStateResponseServiceMock).unexpectedError();

    verify(birthdayRepositoryPortMock).deleteById(anyLong());
    verify(commandStateResponseServiceMock, never()).adminCommandUserRemoved();
    verify(commandStateResponseServiceMock, never()).adminCommandIncorrectUserId();
    verifyNoInteractions(timerTaskScheduler);
  }

  @Test
  void onMessageCreateCalendarEmptyBirthdayListTest() throws IOException {
    when(messageCreateEventMock.getMessageAuthor()).thenReturn(messageAuthorMock);
    when(messageCreateEventMock.isServerMessage()).thenReturn(true);
    when(messageCreateEventMock.getChannel()).thenReturn(textChannelMock);
    when(messageAuthorMock.isRegularUser()).thenReturn(true);
    when(commandStateResponseServiceMock.startGeneratingCalendar())
        .thenReturn(messageBuilderMock);
    when(commandStateResponseServiceMock.sendCalendarAsAttachment(anyString(), any()))
        .thenReturn(messageBuilderMock);

    when(messageCreateEventMock.getServer()).thenReturn(Optional.of(serverMock));
    when(messageCreateEventMock.getMessageContent()).thenReturn("$birthday calendar");
    when(commandServiceMock.parseCommandType("$birthday calendar")).thenReturn(Optional.of(CommandHeaderType.CALENDAR));

    when(birthdayRepositoryPortMock.findAllEnabled()).thenReturn(Collections.emptyList());
    when(calendarBuildServiceMock.buildCalendar(anyList())).thenReturn(new ByteArrayInputStream("test".getBytes()));
    when(userResourceBundleMock.getString("generating.calendar.name")).thenReturn("calendar.pdf");
    botCommandListener.onMessageCreate(messageCreateEventMock);

    verify(commandServiceMock).parseCommandType(anyString());
    verify(commandStateResponseServiceMock).startGeneratingCalendar();
    verify(commandStateResponseServiceMock).sendCalendarAsAttachment(anyString(), any());
    verify(birthdayRepositoryPortMock).findAllEnabled();

    verify(commandStateResponseServiceMock, never()).generatingCalendarError();
    verifyNoInteractions(adminCommandServiceMock, timerTaskScheduler);
  }

  @Test
  void onMessageCreateCalendarCalendarBuildErrorListTest() throws IOException {
    when(messageCreateEventMock.getMessageAuthor()).thenReturn(messageAuthorMock);
    when(messageCreateEventMock.isServerMessage()).thenReturn(true);
    when(messageCreateEventMock.getChannel()).thenReturn(textChannelMock);
    when(messageAuthorMock.isRegularUser()).thenReturn(true);
    when(commandStateResponseServiceMock.startGeneratingCalendar())
        .thenReturn(messageBuilderMock);
    when(commandStateResponseServiceMock.generatingCalendarError())
        .thenReturn(messageBuilderMock);

    when(messageCreateEventMock.getServer()).thenReturn(Optional.of(serverMock));
    when(messageCreateEventMock.getMessageContent()).thenReturn("$birthday calendar");
    when(commandServiceMock.parseCommandType("$birthday calendar")).thenReturn(Optional.of(CommandHeaderType.CALENDAR));

    when(birthdayRepositoryPortMock.findAllEnabled()).thenReturn(Collections.emptyList());
    when(calendarBuildServiceMock.buildCalendar(anyList())).thenThrow(new IOException());
    botCommandListener.onMessageCreate(messageCreateEventMock);

    verify(commandServiceMock).parseCommandType(anyString());
    verify(commandStateResponseServiceMock).startGeneratingCalendar();
    verify(commandStateResponseServiceMock).generatingCalendarError();
    verify(birthdayRepositoryPortMock).findAllEnabled();

    verify(commandStateResponseServiceMock, never()).sendCalendarAsAttachment(anyString(), any());
    verifyNoInteractions(adminCommandServiceMock, timerTaskScheduler);
  }

  @Test
  void onMessageCreateCalendarNonEmptyBirthdayListTest() throws Exception {
    when(messageCreateEventMock.getMessageAuthor()).thenReturn(messageAuthorMock);
    when(messageCreateEventMock.isServerMessage()).thenReturn(true);
    when(messageCreateEventMock.getChannel()).thenReturn(textChannelMock);

    when(messageAuthorMock.isRegularUser()).thenReturn(true);


    when(commandStateResponseServiceMock.startGeneratingCalendar())
        .thenReturn(messageBuilderMock);
    when(commandStateResponseServiceMock.sendCalendarAsAttachment(anyString(), any()))
        .thenReturn(messageBuilderMock);

    when(messageCreateEventMock.getServer()).thenReturn(Optional.of(this.serverMock));
    when(messageCreateEventMock.getMessageContent()).thenReturn("$birthday calendar");
    when(commandServiceMock.parseCommandType("$birthday calendar")).thenReturn(Optional.of(CommandHeaderType.CALENDAR));

    when(birthdayRepositoryPortMock.findAllEnabled()).thenReturn(userMockList());
    when(calendarBuildServiceMock.buildCalendar(anyList())).thenReturn(new ByteArrayInputStream("test".getBytes()));
    when(userResourceBundleMock.getString("generating.calendar.name")).thenReturn("calendar.pdf");
    botCommandListener.onMessageCreate(messageCreateEventMock);

    verify(commandServiceMock).parseCommandType(anyString());
    verify(commandStateResponseServiceMock).startGeneratingCalendar();
    verify(commandStateResponseServiceMock).sendCalendarAsAttachment(anyString(), any());
    verify(birthdayRepositoryPortMock).findAllEnabled();

    verify(commandStateResponseServiceMock, never()).generatingCalendarError();
    verifyNoInteractions(adminCommandServiceMock, timerTaskScheduler);
  }

  private List<Birthday> userMockList() {
    return List.of(Birthday.builder()
            .date(BirthdayDate.builder()
                .year(2000)
                .month(6)
                .day(20)
                .build())
            .user(BirthdayUser.builder()
                .userId(1L)
                .userName("USER-NAME-1")
                .build())
            .build(),
        Birthday.builder()
            .date(BirthdayDate.builder()
                .year(2001)
                .month(9)
                .day(12)
                .build())
            .user(BirthdayUser.builder()
                .userId(2L)
                .userName("USER-NAME-2")
                .build())
            .build());
  }
}