package pl.poweredbychocolate.discord.listener;

import org.javacord.api.entity.user.User;
import org.javacord.api.event.server.member.ServerMemberJoinEvent;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.poweredbychocolate.birthday.domain.Birthday;
import pl.poweredbychocolate.birthday.domain.BirthdayDate;
import pl.poweredbychocolate.birthday.domain.BirthdayUser;
import pl.poweredbychocolate.birthday.repository.BirthdayRepositoryPort;

import java.io.IOException;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class UsersJoinToServerListenerTest {

  @Mock
  private BirthdayRepositoryPort birthdayRepositoryPort;

  @InjectMocks
  private UsersJoinToServerListener usersJoinToServerListener;

  @Test
  void onServerMemberJoinNoSavedUserTest() throws IOException {
    assertNotNull(usersJoinToServerListener);

    ServerMemberJoinEvent serverMemberJoinEventMock = mock(ServerMemberJoinEvent.class);
    User userMock = mock(User.class);

    when(serverMemberJoinEventMock.getUser()).thenReturn(userMock);
    when(userMock.getName()).thenReturn("Mock user name");
    when(userMock.getId()).thenReturn(10L);

    when(birthdayRepositoryPort.findById(10L)).thenReturn(Optional.empty());

    usersJoinToServerListener.onServerMemberJoin(serverMemberJoinEventMock);

    verify(birthdayRepositoryPort).findById(10L);
    verify(birthdayRepositoryPort, never()).replace(any());

  }

  @Test
  void onServerMemberJoinExistsUserTest() throws IOException {
    assertNotNull(usersJoinToServerListener);

    ServerMemberJoinEvent serverMemberJoinEventMock = mock(ServerMemberJoinEvent.class);
    User userMock = mock(User.class);

    when(serverMemberJoinEventMock.getUser()).thenReturn(userMock);
    when(userMock.getName()).thenReturn("Mock user name");
    when(userMock.getId()).thenReturn(20L);

    Birthday birthday = Birthday.builder()
        .user(BirthdayUser.builder()
            .userId(20L)
            .build())
        .date(BirthdayDate.builder()
            .build())
        .build();

    when(birthdayRepositoryPort.findById(20L)).thenReturn(Optional.of(birthday));

    usersJoinToServerListener.onServerMemberJoin(serverMemberJoinEventMock);

    when(birthdayRepositoryPort.replace(any(Birthday.class))).thenThrow(new IOException());
    usersJoinToServerListener.onServerMemberJoin(serverMemberJoinEventMock);

    verify(birthdayRepositoryPort,times(2)).findById(20L);
    verify(birthdayRepositoryPort, times(2)).replace(any());

  }
}