package pl.poweredbychocolate.discord.mapper;

import org.junit.jupiter.api.Test;
import pl.poweredbychocolate.birthday.domain.Birthday;
import pl.poweredbychocolate.birthday.domain.BirthdayDate;
import pl.poweredbychocolate.command.domain.BirthdayAddCommand;
import pl.poweredbychocolate.command.domain.BirthdayWhoCommand;

import static org.junit.jupiter.api.Assertions.*;

class CommandMapperTest {

  @Test
  void toBirthdayTest() {

    BirthdayAddCommand birthdayAddCommand = new BirthdayAddCommand();
    birthdayAddCommand.setDay(5);
    birthdayAddCommand.setMonth(6);
    birthdayAddCommand.setYear(2005);
    birthdayAddCommand.setUserId(100L);
    birthdayAddCommand.setUserName("test user 2");

    Birthday mappedBirthday = CommandMapper.toBirthday(birthdayAddCommand);

    assertNotNull(mappedBirthday);
    assertEquals(100L, mappedBirthday.getUserId());
    assertEquals("test user 2", mappedBirthday.getUserName());
    assertEquals(2005, mappedBirthday.getYear());
    assertEquals(6, mappedBirthday.getMonth());
    assertEquals(5, mappedBirthday.getDay());
  }

  @Test
  void toBirthdayDateTest() {
    BirthdayWhoCommand birthdayWhoCommand = new BirthdayWhoCommand();
    birthdayWhoCommand.setDay(1);
    birthdayWhoCommand.setMonth(2);
    birthdayWhoCommand.setYear(2003);

    BirthdayDate mappedBirthdayDate = CommandMapper.toBirthdayDate(birthdayWhoCommand);

    assertNotNull(mappedBirthdayDate);
    assertEquals(1, mappedBirthdayDate.getDay());
    assertEquals(2, mappedBirthdayDate.getMonth());
    assertEquals(2003, mappedBirthdayDate.getYear());
  }
}