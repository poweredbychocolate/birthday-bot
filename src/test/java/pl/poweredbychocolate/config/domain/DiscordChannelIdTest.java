package pl.poweredbychocolate.config.domain;

import org.junit.jupiter.api.Test;
import pl.poweredbychocolate.config.exception.InvalidDiscordChannelIdException;

import static org.junit.jupiter.api.Assertions.*;

class DiscordChannelIdTest {

  @Test
  void test() {
    assertDoesNotThrow(() -> {
      Long testChannelId = 704007272409464853L;
      DiscordChannelId discordChannelId = DiscordChannelId.of(testChannelId);

      assertTrue(discordChannelId.match(testChannelId));
      assertEquals(testChannelId, discordChannelId.fetch());

      testChannelId = 704007272409464854L;

      assertFalse(discordChannelId.match(testChannelId));
      assertNotEquals(testChannelId, discordChannelId.fetch());
    });

    assertThrows(InvalidDiscordChannelIdException.class, () -> DiscordChannelId.of(null));
  }

}