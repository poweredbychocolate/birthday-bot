package pl.poweredbychocolate.config.domain;

import org.junit.jupiter.api.Test;
import pl.poweredbychocolate.config.exception.InvalidDiscordTokenException;

import static org.junit.jupiter.api.Assertions.*;

class DiscordTokenTest {

  @Test
  void test() {
    assertDoesNotThrow(() -> {
      String testToken = "NzAxqzMwODMnMzAzMTY2NDY2";
      DiscordToken discordToken = DiscordToken.of(testToken);

      assertEquals(testToken, discordToken.fetch());

      testToken = "NzAxqhMwODMnMzAzMTY2NDT2";

      assertNotEquals(testToken, discordToken.fetch());
    });

    assertThrows(InvalidDiscordTokenException.class, () -> DiscordToken.of(null));

    assertThrows(InvalidDiscordTokenException.class, () -> DiscordToken.of(""));
  }
}