package pl.poweredbychocolate.config;

import org.junit.jupiter.api.Test;
import pl.poweredbychocolate.storage.domain.ConfigFile;

import static org.junit.jupiter.api.Assertions.*;

class AppConfigTest {

  @Test
  void test() {
    final ConfigFile configFile = ConfigFile.builder()
        .botToken("test-token")
        .notificationChannelId(12345L)
        .birthdayCheckHour(9)
        .birthdayCheckMinute(15)
        .build();

    AppConfig appConfig = AppConfig.of(configFile);

    assertEquals(configFile.getNotificationChannelId(), appConfig.notificationChannel());
    assertEquals(configFile.getBotToken(), appConfig.token());

  }

}