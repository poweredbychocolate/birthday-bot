package pl.poweredbychocolate.config.service;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.poweredbychocolate.storage.StorageService;

import java.io.IOException;
import java.util.Locale;
import java.util.ResourceBundle;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class GenerateConfigServiceTest {
  @Mock
  private StorageService storageService;

  @Test
  void generate() throws IOException {
    ResourceBundle resourceBundle = ResourceBundle.getBundle("lang", Locale.getDefault());
    final GenerateConfigService generateConfigService = new GenerateConfigService(storageService, resourceBundle);

    doNothing().when(storageService).saveConfigFile(any());
    doNothing().when(storageService).saveUserTemplate(anyString(), any());
    assertDoesNotThrow(generateConfigService::generate);

    verify(storageService).saveConfigFile(any());
    verify(storageService, times(2)).saveUserTemplate(anyString(), any());
  }

}