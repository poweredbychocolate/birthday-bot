package pl.poweredbychocolate.command.service;

import org.junit.jupiter.api.Test;
import pl.poweredbychocolate.command.domain.*;
import pl.poweredbychocolate.command.enums.CommandHeaderType;

import java.util.Locale;
import java.util.Optional;
import java.util.ResourceBundle;

import static org.junit.jupiter.api.Assertions.*;

class CommandServiceTest {
  final Locale userLocale = Locale.getDefault();
  final ResourceBundle userResourceBundle = ResourceBundle.getBundle("lang", userLocale);
  final CommandHeaderService commandHeaderService = new CommandHeaderService(userResourceBundle);
  final CommandHeadersList commandHeadersList = commandHeaderService.buildCommandHeadersList();
  final AdminCommandHeadersList adminCommandHeadersList = commandHeaderService.buildAdminCommandHeadersList();
  final CommandService commandService = new CommandService(commandHeadersList, adminCommandHeadersList);

  final String removeCommandString = userResourceBundle.getString("command.birthday.remove");
  final String addCommandString = userResourceBundle.getString("command.birthday.add");
  final String whoCommandString = userResourceBundle.getString("command.birthday.who");
  final String adminUserRemove = userResourceBundle.getString("command.admin.remove.user");

  @Test
  void parseCommandTypeTest() {
    assertDoesNotThrow(() -> {

      Optional<CommandHeaderType> commandType = commandService.parseCommandType(whoCommandString + " 11");
      assertTrue(commandType.isPresent());
      assertEquals(CommandHeaderType.USER_WHO, commandType.get());

      commandType = commandService.parseCommandType(addCommandString + " 11.03");
      assertTrue(commandType.isPresent());
      assertEquals(CommandHeaderType.USER_ADD, commandType.get());

      commandType = commandService.parseCommandType(removeCommandString);
      assertTrue(commandType.isPresent());
      assertEquals(CommandHeaderType.USER_REMOVE, commandType.get());

      commandType = commandService.parseCommandType(".birthday print");
      assertFalse(commandType.isPresent());
    });
  }

  @Test
  void parseUserAddCommandTest() {
    assertDoesNotThrow(() -> {

      Optional<BirthdayAddCommand> addCommand = commandService.parseUserAddCommand(addCommandString + " 24.04", 1L, "testUser");

      assertTrue(addCommand.isPresent());
      assertEquals(24, addCommand.get().getDay());
      assertEquals(4, addCommand.get().getMonth());
      assertNull(addCommand.get().getYear());
      assertEquals(1L, addCommand.get().getUserId());
      assertEquals("testUser", addCommand.get().getUserName());

      addCommand = commandService.parseUserAddCommand(addCommandString + " 24.04.2001", 1L, "testUser");

      assertTrue(addCommand.isPresent());
      assertEquals(24, addCommand.get().getDay());
      assertEquals(4, addCommand.get().getMonth());
      assertEquals(2001, addCommand.get().getYear());
      assertEquals(1L, addCommand.get().getUserId());
      assertEquals("testUser", addCommand.get().getUserName());

      addCommand = commandService.parseUserAddCommand(addCommandString + " 24.04w", 1L, "testUser");
      assertFalse(addCommand.isPresent());
    });
  }

  @Test
  void parseUserRemoveCommandTest() {
    assertDoesNotThrow(() -> {

      BirthdayRemoveCommand removeCommand = commandService.parseUserRemoveCommand(10234L, "removeTestUser");

      assertNotNull(removeCommand);
      assertEquals(10234L, removeCommand.getUserId());
      assertEquals("removeTestUser", removeCommand.getUserName());
    });
  }

  @Test
  void parseUserWhoCommandTest() {

    assertDoesNotThrow(() -> {
      Optional<BirthdayWhoCommand> whoCommand = commandService.parseUserWhoCommand(whoCommandString + " 10");

      assertTrue(whoCommand.isPresent());
      assertNull(whoCommand.get().getDay());
      assertEquals(10, whoCommand.get().getMonth());
      assertNull(whoCommand.get().getYear());

      whoCommand = commandService.parseUserWhoCommand(whoCommandString + " 2009");

      assertTrue(whoCommand.isPresent());
      assertNull(whoCommand.get().getDay());
      assertNull(whoCommand.get().getMonth());
      assertEquals(2009, whoCommand.get().getYear());

      whoCommand = commandService.parseUserWhoCommand(whoCommandString + " 12.08");

      assertTrue(whoCommand.isPresent());
      assertEquals(12, whoCommand.get().getDay());
      assertEquals(8, whoCommand.get().getMonth());
      assertNull(whoCommand.get().getYear());

      whoCommand = commandService.parseUserWhoCommand(whoCommandString + " 12.2006");

      assertTrue(whoCommand.isPresent());
      assertNull(whoCommand.get().getDay());
      assertEquals(12, whoCommand.get().getMonth());
      assertEquals(2006, whoCommand.get().getYear());

      whoCommand = commandService.parseUserWhoCommand(whoCommandString + " 2.12.2006");

      assertTrue(whoCommand.isPresent());
      assertEquals(2, whoCommand.get().getDay());
      assertEquals(12, whoCommand.get().getMonth());
      assertEquals(2006, whoCommand.get().getYear());

      whoCommand = commandService.parseUserWhoCommand(whoCommandString + " 12.08s");

      assertFalse(whoCommand.isPresent());

      whoCommand = commandService.parseUserWhoCommand(whoCommandString + " 12.08.2s05");

      assertFalse(whoCommand.isPresent());
    });
  }

  @Test
  void parseAdminUserRemoveCommandTest() {

    assertDoesNotThrow(() -> {
      Optional<Long> userToRemoveKey = commandService.parseAdminUserRemoveCommand(adminUserRemove);
      assertTrue(userToRemoveKey.isEmpty());

      userToRemoveKey = commandService.parseAdminUserRemoveCommand(adminUserRemove + " 12A2");
      assertTrue(userToRemoveKey.isEmpty());

      userToRemoveKey = commandService.parseAdminUserRemoveCommand(adminUserRemove + " 1235456");
      assertTrue(userToRemoveKey.isPresent());
      assertEquals(1235456L, userToRemoveKey.get());

    });
  }
}