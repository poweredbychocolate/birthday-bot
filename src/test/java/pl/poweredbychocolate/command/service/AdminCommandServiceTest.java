package pl.poweredbychocolate.command.service;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.poweredbychocolate.command.domain.AdminCommandHeadersList;
import pl.poweredbychocolate.command.enums.AdminCommandHeaderType;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class AdminCommandServiceTest {

  @Test
  void parseCommandType() {
    AdminCommandHeadersList adminCommandHeadersList = new AdminCommandHeadersList();
    adminCommandHeadersList.applyCommand("rm", AdminCommandHeaderType.REMOVE_USER);

    AdminCommandHeadersList adminCommandHeadersListSpy = Mockito.spy(adminCommandHeadersList);
    final AdminCommandService adminCommandService = new AdminCommandService(adminCommandHeadersListSpy, Collections.emptyList());

    Optional<AdminCommandHeaderType> parsedCommand = adminCommandService.parseCommandType("add user1");
    assertTrue(parsedCommand.isEmpty());

    parsedCommand = adminCommandService.parseCommandType("rm user1");
    assertTrue(parsedCommand.isPresent());
    assertEquals(AdminCommandHeaderType.REMOVE_USER, parsedCommand.get());
  }

  @Test
  void hasPermission() {

    AdminCommandHeadersList adminCommandHeadersList = Mockito.mock(AdminCommandHeadersList.class);
    List<Long> permissionList = List.of(123L, 234L);
    final AdminCommandService adminCommandService = new AdminCommandService(adminCommandHeadersList, permissionList);

    assertDoesNotThrow(() -> {
      assertFalse(adminCommandService.hasPermission(1234L));
      assertTrue(adminCommandService.hasPermission(123L));
      assertTrue(adminCommandService.hasPermission(234L));
    });

    verifyNoInteractions(adminCommandHeadersList);
  }
}