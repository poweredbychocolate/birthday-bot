package pl.poweredbychocolate.adapter;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.poweredbychocolate.birthday.domain.Birthday;
import pl.poweredbychocolate.birthday.domain.BirthdayDate;
import pl.poweredbychocolate.storage.domain.YamlFile;

import java.util.*;
import java.util.stream.Collectors;

@AllArgsConstructor
@NoArgsConstructor
@Data
class BirthdayStorage implements YamlFile {
  HashMap<Long, Birthday> birthdayHashMap;

  public boolean add(Long userId, Birthday birthday) {
    return Objects.isNull(birthdayHashMap.putIfAbsent(userId, birthday));
  }

  public boolean replace(Long userId, Birthday birthday) {
    if (birthdayHashMap.containsKey(userId)) {
      birthdayHashMap.replace(userId, birthday);
      return true;
    }
    return false;
  }

  public boolean remove(Long userId) {
    return Objects.nonNull(birthdayHashMap.remove(userId));
  }

  @JsonIgnore
  public List<Birthday> getAll() {
    return birthdayHashMap.values()
        .stream()
        .filter(Birthday::isEnabled)
        .collect(Collectors.toList());
  }

  public List<Birthday> getBirthdayByBirthdayDate(BirthdayDate inputBirthdayDate) {
    return birthdayHashMap.values()
        .stream()
        .filter(birthday -> filterByBirthdayDateAndEnabledStatus(birthday, inputBirthdayDate))
        .collect(Collectors.toList());

  }

  private boolean filterByBirthdayDateAndEnabledStatus(Birthday birthday, BirthdayDate inputBirthdayDate) {
    return birthday.isEnabled()
        && isBirthdayDatePartEquals(birthday.getDay(), inputBirthdayDate.getDay())
        && isBirthdayDatePartEquals(birthday.getMonth(), inputBirthdayDate.getMonth())
        && isBirthdayDatePartEquals(birthday.getYear(), inputBirthdayDate.getYear());
  }

  private boolean isBirthdayDatePartEquals(Integer datePart, Integer inputCondition) {
    return Objects.isNull(inputCondition) || inputCondition.equals(datePart);
  }

  public Optional<Birthday> findById(Long userId) {
    return Optional.ofNullable(birthdayHashMap.get(userId));
  }
}
