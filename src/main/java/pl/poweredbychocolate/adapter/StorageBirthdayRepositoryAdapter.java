package pl.poweredbychocolate.adapter;

import lombok.extern.slf4j.Slf4j;
import pl.poweredbychocolate.birthday.domain.Birthday;
import pl.poweredbychocolate.birthday.domain.BirthdayDate;
import pl.poweredbychocolate.birthday.domain.BirthdayUser;
import pl.poweredbychocolate.birthday.repository.BirthdayRepositoryPort;
import pl.poweredbychocolate.storage.repository.StorageRepository;

import java.io.IOException;
import java.nio.file.Path;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Optional;

@Slf4j
public class StorageBirthdayRepositoryAdapter implements BirthdayRepositoryPort {
  private final BirthdayStorage birthdayStorage;
  private final StorageRepository storageRepository;
  private final Path storagePath = Path.of("db.yml");

  public StorageBirthdayRepositoryAdapter(StorageRepository storageRepository) throws IOException {
    this.storageRepository = storageRepository;
    this.birthdayStorage = initBirthdayStorage();
  }

  private BirthdayStorage initBirthdayStorage() throws IOException {
    if (storageRepository.isFileExist(storagePath)) {
      return storageRepository.readYAMLFromFile(storagePath, BirthdayStorage.class);
    } else {
      return new BirthdayStorage(new LinkedHashMap<>());
    }
  }

  @Override
  public boolean save(Birthday birthday) throws IOException {
    boolean saveState = birthdayStorage.add(birthday.getUserId(), birthday);
    handleBirthdayStorageChange(saveState);
    log.debug("Save {} operation status: {}", birthday, saveState);
    return saveState;
  }

  @Override
  public boolean replace(Birthday birthday) throws IOException {
    boolean replaceState = birthdayStorage.replace(birthday.getUserId(), birthday);
    handleBirthdayStorageChange(replaceState);
    log.debug("Replace {} operation status: {}", birthday, replaceState);
    return replaceState;
  }

  @Override
  public boolean delete(BirthdayUser birthdayUser) throws IOException {
    boolean removeState = birthdayStorage.remove(birthdayUser.getUserId());
    handleBirthdayStorageChange(removeState);
    log.debug("Delete {} operation status: {}", birthdayUser, removeState);
    return removeState;
  }

  private void handleBirthdayStorageChange(boolean changed) throws IOException {
    if (changed) {
      storageRepository.saveYamlToFile(birthdayStorage, storagePath);
    }
  }

  @Override
  public List<Birthday> findBirthdayByBirthdayDateIfEnabled(BirthdayDate birthdayDate) {
    return birthdayStorage.getBirthdayByBirthdayDate(birthdayDate);
  }

  @Override
  public List<Birthday> findAllEnabled() {
    return birthdayStorage.getAll();
  }

  @Override
  public Optional<Birthday> findById(Long userId) {
    return birthdayStorage.findById(userId);
  }

  @Override
  public boolean deleteById(Long userToRemoveId) throws IOException {
    boolean removeState = birthdayStorage.remove(userToRemoveId);
    handleBirthdayStorageChange(removeState);
    log.debug("Delete by id = {} operation status: {}", userToRemoveId, removeState);
    return removeState;
  }
}
