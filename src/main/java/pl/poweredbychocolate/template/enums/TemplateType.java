package pl.poweredbychocolate.template.enums;

public enum TemplateType {
  SINGLE,
  MULTIPLE
}
