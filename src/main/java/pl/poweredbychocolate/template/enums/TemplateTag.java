package pl.poweredbychocolate.template.enums;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public enum TemplateTag {
  USER_NAME("${user.name}"),
  USER_AGE("${user.age}"),
  IF_AGE("${if.age}"),
  LOOP("${loop}"),
  IF_AGE_REGEX(" ?\\$\\{if.age}"),
  LOOP_REGEX(" ?\\$\\{loop}");

  private final String tag;

  public String tag() {
    return tag;
  }

  public int length() {
    return tag.length();
  }
}
