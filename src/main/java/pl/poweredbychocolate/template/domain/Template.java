package pl.poweredbychocolate.template.domain;

import lombok.Builder;
import lombok.Value;
import pl.poweredbychocolate.template.enums.TemplateType;

@Value
@Builder
public class Template {
  TemplateType type;
  String content;
}
