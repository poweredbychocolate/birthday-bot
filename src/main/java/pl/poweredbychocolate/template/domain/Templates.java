package pl.poweredbychocolate.template.domain;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class Templates {
  Template singleTemplate;
  Template multipleTemplate;
}
