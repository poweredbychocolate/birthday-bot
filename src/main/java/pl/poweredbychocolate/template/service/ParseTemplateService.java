package pl.poweredbychocolate.template.service;

import pl.poweredbychocolate.birthday.domain.Birthday;
import pl.poweredbychocolate.template.domain.Template;
import pl.poweredbychocolate.template.enums.TemplateTag;

import java.time.LocalDate;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class ParseTemplateService {

  public String parseTemplate(Template template, Birthday birthday) {
    var userMention = "<@" + birthday.getUserId() + ">";
    var parsedTemplateContent = template.getContent()
        .replace(TemplateTag.USER_NAME.tag(), userMention);

    return parseUserAge(parsedTemplateContent, birthday);
  }

  private String parseUserAge(String inputTemplateContent, Birthday birthday) {
    if (inputTemplateContent.contains(TemplateTag.USER_AGE.tag())) {
      if (Objects.nonNull(birthday.getYear())) {
        Integer userAge = calculateUserAge(birthday);
        return parseIfAgeTag(inputTemplateContent, userAge);
      } else {
        return clearIfAgeTag(inputTemplateContent);
      }
    }
    return inputTemplateContent;
  }

  private String parseIfAgeTag(String inputContent, Integer userAge) {
    return inputContent.replaceAll(TemplateTag.IF_AGE_REGEX.tag(), "")
        .replace(TemplateTag.USER_AGE.tag(), userAge.toString());
  }


  private String clearIfAgeTag(String inputContent) {
    int startIndex = inputContent.indexOf(TemplateTag.IF_AGE.tag());
    int endIndex = inputContent.indexOf(TemplateTag.IF_AGE.tag(), startIndex + TemplateTag.IF_AGE.length());

    String clearedTemplateContent = startIndex != -1
        ? inputContent.substring(0, startIndex)
        : inputContent;

    if (endIndex != -1) {
      clearedTemplateContent += clearIfAgeTag(inputContent.substring(endIndex + TemplateTag.IF_AGE.length()));
    }
    return clearedTemplateContent;
  }

  private Integer calculateUserAge(Birthday birthday) {
    return LocalDate.now().getYear() - birthday.getYear();
  }

  public String parseTemplate(Template template, List<Birthday> birthdayList) {
    if (birthdayList.isEmpty())
      return "";
    int loopStartIndex = template.getContent().indexOf(TemplateTag.LOOP.tag());
    int loopEndIndex = template.getContent().indexOf(TemplateTag.LOOP.tag(), loopStartIndex + TemplateTag.IF_AGE.length());
    if (loopStartIndex != -1 && loopEndIndex != -1) {

      var content = template.getContent().substring(0, loopStartIndex)
          + parseLoopTag(template.getContent().substring(loopStartIndex, loopEndIndex), birthdayList)
          + template.getContent().substring(loopEndIndex + TemplateTag.LOOP.length());
      return content.replaceAll("\\s{2,}", " ");
    }
    return template.getContent();
  }

  private String parseLoopTag(String inputTemplateContent, List<Birthday> birthdayList) {
    final var clearedTemplate = inputTemplateContent.replaceAll(TemplateTag.LOOP_REGEX.tag(), "");

    return birthdayList.stream()
        .map(birthdayItem -> parseLoopPart(birthdayItem, clearedTemplate))
        .collect(Collectors.joining());
  }

  private String parseLoopPart(Birthday birthdayItem, String clearedTemplate) {
    var userMention = "<@" + birthdayItem.getUserId() + ">";
    var parsedPartContext = clearedTemplate.replace(TemplateTag.USER_NAME.tag(), userMention);
    if (Objects.nonNull(birthdayItem.getYear())) {
      Integer userAge = calculateUserAge(birthdayItem);
      return parseIfAgeTag(parsedPartContext, userAge);
    }
    return clearIfAgeTag(parsedPartContext);
  }
}
