package pl.poweredbychocolate.template.service;

import lombok.RequiredArgsConstructor;
import pl.poweredbychocolate.config.enums.TemplateName;
import pl.poweredbychocolate.storage.StorageService;
import pl.poweredbychocolate.template.domain.Template;
import pl.poweredbychocolate.template.domain.Templates;
import pl.poweredbychocolate.template.enums.TemplateType;

import java.io.IOException;

@RequiredArgsConstructor
public class LoadTemplateService {
  private final StorageService storageService;

  public Templates loadTemplates() throws IOException {

    var singleTemplateContent = storageService.readUserTemplate(TemplateName.SINGLE_USER_TEMPLATE.path());
    var multipleTemplateContent = storageService.readUserTemplate(TemplateName.MULTI_USER_TEMPLATE.path());

    var singleTemplate = Template.builder()
        .type(TemplateType.SINGLE)
        .content(singleTemplateContent)
        .build();

    var multipleTemplate = Template.builder()
        .type(TemplateType.MULTIPLE)
        .content(multipleTemplateContent)
        .build();

    return Templates.builder()
        .singleTemplate(singleTemplate)
        .multipleTemplate(multipleTemplate)
        .build();
  }
}
