package pl.poweredbychocolate.calendar.service;

import com.itextpdf.io.font.PdfEncodings;
import com.itextpdf.io.image.ImageData;
import com.itextpdf.io.image.ImageDataFactory;
import com.itextpdf.kernel.colors.Color;
import com.itextpdf.kernel.colors.ColorConstants;
import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.geom.Rectangle;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.kernel.pdf.canvas.PdfCanvas;
import com.itextpdf.kernel.pdf.xobject.PdfFormXObject;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.borders.Border;
import com.itextpdf.layout.element.*;
import com.itextpdf.layout.properties.HorizontalAlignment;
import com.itextpdf.layout.properties.TextAlignment;
import com.itextpdf.layout.properties.UnitValue;
import lombok.RequiredArgsConstructor;
import pl.poweredbychocolate.calendar.domain.CalendarUser;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.Month;
import java.time.Year;
import java.time.format.TextStyle;
import java.util.Comparator;
import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public class CalendarBuildService {
  private static final String FONT_PATH = "src/main/resources/fonts/LiberationSans.ttf";
  private static final Float AVATAR_HEIGHT = 32F;
  private static final Float AVATAR_WIDTH = 32F;
  private static final int FIRST_DAY = 1;

  private final ResourceBundle userResourceBundle;

  public InputStream buildCalendar(List<CalendarUser> calendarUserList) throws IOException {
    PdfFont calendarFont = PdfFontFactory.createFont(FONT_PATH, PdfEncodings.IDENTITY_H, PdfFontFactory.EmbeddingStrategy.PREFER_EMBEDDED, true);
    ByteArrayOutputStream calendarOutputStream = new ByteArrayOutputStream();

    PdfWriter calendarPdfWriter = new PdfWriter(calendarOutputStream);
    PdfDocument calendarPdfDocument = new PdfDocument(calendarPdfWriter);

    Document calendarDocument = new Document(calendarPdfDocument, PageSize.A4.rotate())
        .setFont(calendarFont);

    buildMonthsPages(calendarUserList, calendarDocument);

    calendarDocument.close();
    InputStream calendarAsInputStream = new ByteArrayInputStream(calendarOutputStream.toByteArray());
    calendarDocument.close();
    calendarOutputStream.close();
    return calendarAsInputStream;
  }


  void buildMonthsPages(List<CalendarUser> calendarUserList, Document calendarDocument) {

    for (Month month : Month.values()) {
      List<CalendarUser> currentMonthUsers = findUserForCurrentMonth(month, calendarUserList);
      buildAndAppendMonthPage(month, currentMonthUsers, calendarDocument);
    }
  }

  private List<CalendarUser> findUserForCurrentMonth(Month month, List<CalendarUser> calendarUserList) {
    return calendarUserList.stream()
        .filter(calendarUser -> calendarUser.getMonth().equals(month.getValue()))
        .collect(Collectors.toList());
  }

  private void buildAndAppendMonthPage(Month month, List<CalendarUser> currentMonthUsers, Document calendarDocument) {
    Paragraph monthHeaderParagraph = buildMonthHeaderParagraph(month);
    calendarDocument.add(monthHeaderParagraph);

    Table monthTable = new Table(7);
    monthTable.setWidth(new UnitValue(UnitValue.PERCENT, 100));

    tableAppendDayHeaderWithSelectedOrder(monthTable);
    tableAppendDaysCells(monthTable, month, currentMonthUsers, calendarDocument);

    calendarDocument.add(monthTable);
    if (!Month.DECEMBER.equals(month)) {
      calendarDocument.add(new AreaBreak());
    }
  }

  private Paragraph buildMonthHeaderParagraph(Month month) {
    String monthHeader = month.getDisplayName(TextStyle.FULL_STANDALONE, userResourceBundle.getLocale())
        .toUpperCase(userResourceBundle.getLocale());

    Text monthHeaderText = new Text(monthHeader)
        .setFontColor(ColorConstants.ORANGE);
    return new Paragraph(monthHeaderText).setTextAlignment(TextAlignment.CENTER);
  }

  private void tableAppendDayHeaderWithSelectedOrder(Table table) {
    table.addCell(buildWorkDayHeaderCell(formatDayOfWeekName(DayOfWeek.MONDAY)));
    table.addCell(buildWorkDayHeaderCell(formatDayOfWeekName(DayOfWeek.TUESDAY)));
    table.addCell(buildWorkDayHeaderCell(formatDayOfWeekName(DayOfWeek.WEDNESDAY)));
    table.addCell(buildWorkDayHeaderCell(formatDayOfWeekName(DayOfWeek.THURSDAY)));
    table.addCell(buildWorkDayHeaderCell(formatDayOfWeekName(DayOfWeek.FRIDAY)));
    table.addCell(buildSaturdayHeaderCell());
    table.addCell(buildSundayHeaderCell());
  }

  private String formatDayOfWeekName(DayOfWeek dayOfWeek) {
    return dayOfWeek.getDisplayName(TextStyle.FULL_STANDALONE, userResourceBundle.getLocale())
        .toUpperCase(userResourceBundle.getLocale());
  }

  private Cell buildWorkDayHeaderCell(String monthName) {
    return new Cell()
        .setFontColor(ColorConstants.BLACK)
        .setBackgroundColor(ColorConstants.LIGHT_GRAY)
        .setHorizontalAlignment(HorizontalAlignment.CENTER)
        .add(new Paragraph(monthName));
  }

  private Cell buildSaturdayHeaderCell() {
    return new Cell()
        .setFontColor(ColorConstants.BLUE)
        .setBackgroundColor(ColorConstants.LIGHT_GRAY)
        .setHorizontalAlignment(HorizontalAlignment.CENTER)
        .add(new Paragraph(formatDayOfWeekName(DayOfWeek.SATURDAY)));
  }

  private Cell buildSundayHeaderCell() {
    return new Cell()
        .setFontColor(ColorConstants.RED)
        .setBackgroundColor(ColorConstants.LIGHT_GRAY)
        .setHorizontalAlignment(HorizontalAlignment.CENTER)
        .add(new Paragraph(formatDayOfWeekName(DayOfWeek.SUNDAY)));
  }

  private void tableAppendDaysCells(Table table, Month month, List<CalendarUser> currentMonthUsers, Document calendarDocument) {
    tableAppendEmptyCellsAtMonthBegin(table, month);
    tableAppendCells(table, month, currentMonthUsers, calendarDocument);
    tableAppendEmptyCellsAtMonthEnd(table, month);
  }

  private void tableAppendEmptyCellsAtMonthBegin(Table table, Month month) {
    DayOfWeek monthStartDayOfWeek = Year.now().atMonth(month).atDay(FIRST_DAY).getDayOfWeek();

    for (DayOfWeek dayOfWeek : DayOfWeek.values()) {
      if (dayOfWeek.equals(monthStartDayOfWeek)) {
        break;
      }
      table.addCell(new Cell());
    }
  }

  private void tableAppendCells(Table table, Month month, List<CalendarUser> currentMonthUsers, Document calendarDocument) {
    final Year currentYear = Year.now();
    int monthDaysCount = month.length(currentYear.isLeap());

    for (int monthDay = 1; monthDay <= monthDaysCount; monthDay++) {
      LocalDate monthDayLocalDate = currentYear.atMonth(month).atDay(monthDay);

      List<CalendarUser> currentDayUsers = currentMonthUsers.stream()
          .filter(user -> user.getDay().equals(monthDayLocalDate.getDayOfMonth()))
          .sorted(Comparator.comparing(CalendarUser::getUserName))
          .collect(Collectors.toList());

      table.addCell(buildMonthCell(monthDayLocalDate, currentDayUsers, calendarDocument));
    }
  }

  private Cell buildMonthCell(LocalDate monthDayLocalDate, List<CalendarUser> currentDayUsers, Document calendarDocument) {
    Cell monthCell = new Cell();
    Text dayNumberText = new Text(String.valueOf(monthDayLocalDate.getDayOfMonth()));
    Paragraph dayNumberParagraph = new Paragraph(dayNumberText);

    if (currentDayUsers.isEmpty()) {
      monthCell.add(dayNumberParagraph);
      monthCell.setFontColor(getFontColorByLocalDateDayOfWeek(monthDayLocalDate));
    } else {
      Table singleCellTable = buildDayUsersTable(dayNumberParagraph, monthDayLocalDate, currentDayUsers, calendarDocument);
      monthCell.add(singleCellTable);
    }

    return monthCell;
  }

  private Color getFontColorByLocalDateDayOfWeek(LocalDate monthDayLocalDate) {
    switch (monthDayLocalDate.getDayOfWeek()) {
      case MONDAY:
      case THURSDAY:
      case WEDNESDAY:
      case TUESDAY:
      case FRIDAY:
        return ColorConstants.BLACK;
      case SATURDAY:
        return ColorConstants.BLUE;
      case SUNDAY:
        return ColorConstants.RED;
    }
    return ColorConstants.BLACK;
  }

  private Table buildDayUsersTable(Paragraph dayNumberParagraph, LocalDate monthDayLocalDate, List<CalendarUser> currentDayUsers, Document calendarDocument) {
    Table singleDayTable = new Table(2);
    singleDayTable.setWidth(new UnitValue(UnitValue.PERCENT, 100));
    singleDayTable.setBorder(Border.NO_BORDER);

    Cell dayNumberCell = new Cell(1, 2)
        .setBorder(Border.NO_BORDER)
        .setFontColor(getFontColorByLocalDateDayOfWeek(monthDayLocalDate))
        .add(dayNumberParagraph);
    singleDayTable.addCell(dayNumberCell);

    for (CalendarUser user : currentDayUsers) {
      Cell userAvatarCell = buildUserAvatarCell(user, calendarDocument);
      singleDayTable.addCell(userAvatarCell);

      Cell userNameCell = buildUserNameCell(user);
      singleDayTable.addCell(userNameCell);
    }
    return singleDayTable;
  }

  private Cell buildUserAvatarCell(CalendarUser user, Document calendarDocument) {
    ImageData userAvatarImageData = ImageDataFactory.create(user.getAvatar());
    Image circleUserAvatar = makeAvatarWithCircle(userAvatarImageData, calendarDocument);

    return new Cell()
        .setBorder(Border.NO_BORDER)
        .add(circleUserAvatar);
  }

  private Image makeAvatarWithCircle(ImageData userAvatarImageData, Document calendarDocument) {
    PdfFormXObject userAvatarImageInEclipse = new PdfFormXObject(new Rectangle(AVATAR_WIDTH, AVATAR_HEIGHT));
    PdfCanvas userAvatarImageInEclipseCanvas = new PdfCanvas(userAvatarImageInEclipse, calendarDocument.getPdfDocument());
    userAvatarImageInEclipseCanvas.ellipse(0, 0, AVATAR_WIDTH, AVATAR_HEIGHT);
    userAvatarImageInEclipseCanvas.clip();
    userAvatarImageInEclipseCanvas.endPath();
    userAvatarImageInEclipseCanvas.addImageWithTransformationMatrix(userAvatarImageData, AVATAR_WIDTH, 0, 0, AVATAR_HEIGHT, 0, 0);
    return new Image(userAvatarImageInEclipse);
  }

  private Cell buildUserNameCell(CalendarUser user) {
    Text userNameText = new Text(user.getUserName());
    Paragraph userNameParagraph = new Paragraph(userNameText);
    return new Cell()
        .setBorder(Border.NO_BORDER)
        .add(userNameParagraph);
  }

  private void tableAppendEmptyCellsAtMonthEnd(Table table, Month month) {
    Year currentYear = Year.now();

    DayOfWeek monthEndDayOfWeek = currentYear.atMonth(month)
        .atDay(month.length(currentYear.isLeap()))
        .getDayOfWeek();

    for (int dayIndex = monthEndDayOfWeek.getValue(); dayIndex < DayOfWeek.SUNDAY.getValue(); dayIndex++) {
      table.addCell(new Cell());
    }
  }
}
