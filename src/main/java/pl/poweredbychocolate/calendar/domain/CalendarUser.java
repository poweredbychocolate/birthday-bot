package pl.poweredbychocolate.calendar.domain;

import lombok.Builder;
import lombok.Value;

@Builder
@Value
public class CalendarUser {
  Long userId;
  String userName;
  Integer day;
  Integer month;
  byte[] avatar;
}
