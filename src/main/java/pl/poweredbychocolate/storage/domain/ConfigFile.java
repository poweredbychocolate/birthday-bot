package pl.poweredbychocolate.storage.domain;

import lombok.*;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ConfigFile implements YamlFile {
  String botToken;
  Long notificationChannelId;
  Integer birthdayCheckHour;
  Integer birthdayCheckMinute;
  List<Long> adminUserIds;
}
