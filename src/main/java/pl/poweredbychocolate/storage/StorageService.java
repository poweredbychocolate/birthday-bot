package pl.poweredbychocolate.storage;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import pl.poweredbychocolate.storage.domain.ConfigFile;
import pl.poweredbychocolate.storage.repository.StorageRepository;

import java.io.IOException;
import java.nio.file.Path;

@RequiredArgsConstructor
@Slf4j
public class StorageService {
  private final StorageRepository storageRepository;

  public void saveConfigFile(ConfigFile configFileTemplate) throws IOException {
    final Path configFileTemplatePath = Path.of("app-config.yml");
    storageRepository.saveYamlToFile(configFileTemplate, configFileTemplatePath);
  }

  public void saveUserTemplate(String userTemplateText, Path userTemplatePath) throws IOException {
    storageRepository.saveTextToFile(userTemplateText, userTemplatePath);
  }

  public ConfigFile readConfigFile() throws IOException {
    final Path configFileTemplatePath = Path.of("app-config.yml");
    return storageRepository.readYAMLFromFile(configFileTemplatePath, ConfigFile.class);
  }

  public String readUserTemplate(Path path) throws IOException {
    return storageRepository.readTextFromFile(path);
  }
}
