package pl.poweredbychocolate.storage.repository;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import pl.poweredbychocolate.storage.domain.YamlFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class StorageRepository {
  private final ObjectMapper yamlObjectMapper = new ObjectMapper(new YAMLFactory());

  public boolean isFileExist(Path pathToCheck) {
    return Files.exists(pathToCheck);
  }

  public void saveTextToFile(String textToSave, Path pathToFile) throws IOException {
    Files.writeString(pathToFile, textToSave);
  }

  public String readTextFromFile(Path pathToRead) throws IOException {
    return Files.readString(pathToRead);
  }

  public void saveYamlToFile(YamlFile yamlToSave, Path pathToFile) throws IOException {
      yamlObjectMapper.writeValue(pathToFile.toFile(), yamlToSave);
  }

  public <T extends YamlFile> T readYAMLFromFile(Path pathToRead, Class<T> valueType) throws IOException {
    return yamlObjectMapper.readValue(pathToRead.toFile(), valueType);
  }

}
