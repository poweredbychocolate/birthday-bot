package pl.poweredbychocolate.birthday.repository;

import pl.poweredbychocolate.birthday.domain.Birthday;
import pl.poweredbychocolate.birthday.domain.BirthdayDate;
import pl.poweredbychocolate.birthday.domain.BirthdayUser;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

public interface BirthdayRepositoryPort {

  boolean save(Birthday birthday) throws IOException;

  boolean replace(Birthday birthday) throws IOException;

  boolean delete(BirthdayUser birthdayUser) throws IOException;

  List<Birthday> findBirthdayByBirthdayDateIfEnabled(BirthdayDate birthdayDate);

  List<Birthday> findAllEnabled();

  Optional<Birthday> findById(Long userId);

  boolean deleteById(Long userToRemoveId) throws IOException;
}
