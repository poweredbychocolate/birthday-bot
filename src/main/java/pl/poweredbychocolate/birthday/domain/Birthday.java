package pl.poweredbychocolate.birthday.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Optional;


@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Birthday {
  private BirthdayUser user;
  private BirthdayDate date;


  @JsonIgnore
  public Long getUserId() {
    return user.getUserId();
  }

  @JsonIgnore
  public Integer getDay() {
    return date.getDay();
  }

  @JsonIgnore
  public Integer getMonth() {
    return date.getMonth();
  }

  @JsonIgnore
  public Integer getYear() {
    return date.getYear();
  }

  @JsonIgnore
  public String getUserName() {
    return user.getUserName();
  }

  public void setUserNickname(String nickname) {
    user.setUserNickname(nickname);
  }

  @JsonIgnore
  public String getUserNicknameOrName() {
    return Optional.ofNullable(user.getUserNickname()).orElse(user.getUserName());
  }

  public void setUserName(String newUserName) {
    user.setUserName(newUserName);
  }

  @JsonIgnore
  public void disable() {
    user.setEnabled(false);
  }

  @JsonIgnore
  public void enable() {
    user.setEnabled(true);
  }

  @JsonIgnore
  public boolean isEnabled() {
    return user.isEnabled();
  }
}
