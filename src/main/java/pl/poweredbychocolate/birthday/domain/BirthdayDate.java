package pl.poweredbychocolate.birthday.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class BirthdayDate {
  Integer day;
  Integer month;
  Integer year;
}
