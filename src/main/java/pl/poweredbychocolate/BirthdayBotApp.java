package pl.poweredbychocolate;

import lombok.extern.slf4j.Slf4j;
import pl.poweredbychocolate.adapter.StorageBirthdayRepositoryAdapter;
import pl.poweredbychocolate.auto.task.DailyCheckBirthdayTask;
import pl.poweredbychocolate.auto.task.TimerTaskScheduler;
import pl.poweredbychocolate.birthday.repository.BirthdayRepositoryPort;
import pl.poweredbychocolate.calendar.service.CalendarBuildService;
import pl.poweredbychocolate.command.domain.AdminCommandHeadersList;
import pl.poweredbychocolate.command.domain.CommandHeadersList;
import pl.poweredbychocolate.command.service.AdminCommandService;
import pl.poweredbychocolate.command.service.CommandHeaderService;
import pl.poweredbychocolate.command.service.CommandService;
import pl.poweredbychocolate.config.AppConfig;
import pl.poweredbychocolate.config.enums.AppOperation;
import pl.poweredbychocolate.config.service.GenerateConfigService;
import pl.poweredbychocolate.discord.exception.DiscordLoginFailException;
import pl.poweredbychocolate.discord.listener.*;
import pl.poweredbychocolate.discord.service.CommandStateResponseService;
import pl.poweredbychocolate.discord.service.DiscordService;
import pl.poweredbychocolate.storage.StorageService;
import pl.poweredbychocolate.storage.domain.ConfigFile;
import pl.poweredbychocolate.storage.repository.StorageRepository;
import pl.poweredbychocolate.template.domain.Templates;
import pl.poweredbychocolate.template.service.LoadTemplateService;
import pl.poweredbychocolate.template.service.ParseTemplateService;

import java.io.IOException;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.Scanner;

@Slf4j
public class BirthdayBotApp {
  public static void main(String[] args) throws IOException, DiscordLoginFailException {

    log.info("Birthday Bot");
    if (isGenConfigOperation(args)) {
      Locale userLocale = Locale.getDefault();
      ResourceBundle userResourceBundle = ResourceBundle.getBundle("lang", userLocale);
      genConfigFiles(userResourceBundle);
    } else {
      ResourceBundle userResourceBundle = getResourceBundle(args);
      runBirthdayBot(userResourceBundle);
    }
  }

  private static ResourceBundle getResourceBundle(String[] args) {
    Locale userLocale;
    try {
      userLocale = Locale.forLanguageTag(args[0]);
    } catch (Exception parseLanguageArgException) {
      userLocale = Locale.getDefault();
    }
    return ResourceBundle.getBundle("lang", userLocale);
  }

  private static void genConfigFiles(ResourceBundle userResourceBundle) throws IOException {
    StorageRepository storageRepository = new StorageRepository();
    StorageService storageService = new StorageService(storageRepository);

    GenerateConfigService generateConfigService = new GenerateConfigService(storageService, userResourceBundle);
    generateConfigService.generate();
  }

  private static boolean isGenConfigOperation(String[] args) {
    return args.length == 1
        && AppOperation.GENERATE_CONFIG.getOperation().equals(args[0]);
  }

  private static void runBirthdayBot(ResourceBundle userResourceBundle) throws IOException, DiscordLoginFailException {
    final StorageRepository storageRepository = new StorageRepository();
    final StorageService storageService = new StorageService(storageRepository);

    final ConfigFile configFile = storageService.readConfigFile();
    final AppConfig appConfig = AppConfig.of(configFile);

    final CommandHeaderService commandHeaderService = new CommandHeaderService(userResourceBundle);
    final CommandHeadersList commandHeadersList = commandHeaderService.buildCommandHeadersList();
    final AdminCommandHeadersList adminCommandHeadersList = commandHeaderService.buildAdminCommandHeadersList();
    final CommandService commandService = new CommandService(commandHeadersList, adminCommandHeadersList);
    final AdminCommandService adminCommandService = new AdminCommandService(adminCommandHeadersList, appConfig.getUserIdsWithAdminPermissionList());

    final CommandStateResponseService commandStateResponseService = new CommandStateResponseService(userResourceBundle);
    final BirthdayRepositoryPort birthdayRepositoryPort = new StorageBirthdayRepositoryAdapter(storageRepository);
    final TimerTaskScheduler timerTaskScheduler = new TimerTaskScheduler();
    final CalendarBuildService calendarBuildService = new CalendarBuildService(userResourceBundle);

    final BotCommandListener botCommandListener = new BotCommandListener(commandService, birthdayRepositoryPort, commandStateResponseService, adminCommandService, timerTaskScheduler, calendarBuildService, userResourceBundle);
    final UsersNameListener usersNameListener = new UsersNameListener(birthdayRepositoryPort);
    final UsersNicknameListener usersNicknameListener = new UsersNicknameListener(birthdayRepositoryPort);
    final UsersLeaveServerListener usersLeaveServerListener = new UsersLeaveServerListener(birthdayRepositoryPort);
    final UsersJoinToServerListener usersJoinToServerListener = new UsersJoinToServerListener(birthdayRepositoryPort);

    final DiscordServiceListenersPack discordServiceListenersPack = DiscordServiceListenersPack.builder()
        .botCommandListener(botCommandListener)
        .usersNicknameListener(usersNicknameListener)
        .usersNameListener(usersNameListener)
        .usersJoinToServerListener(usersJoinToServerListener)
        .usersLeaveListener(usersLeaveServerListener)
        .build();

    try (DiscordService discordService = new DiscordService(appConfig, discordServiceListenersPack)) {

      final ParseTemplateService parseTemplateService = new ParseTemplateService();
      final LoadTemplateService loadTemplateService = new LoadTemplateService(storageService);
      final Templates templates = loadTemplateService.loadTemplates();

      final DailyCheckBirthdayTask dailyCheckBirthdayTask = new DailyCheckBirthdayTask(discordService, birthdayRepositoryPort, parseTemplateService, templates);

      timerTaskScheduler.schedule(dailyCheckBirthdayTask, appConfig.dailyCheckBirthdayDate());

      log.info("Birthday Bot Online");
      Scanner scanner = new Scanner(System.in);
      while (true) {
        if (scanner.next().equalsIgnoreCase("exit")) break;
      }
      timerTaskScheduler.close();
    }
    log.info("Birthday bot disconnected");
  }
}
