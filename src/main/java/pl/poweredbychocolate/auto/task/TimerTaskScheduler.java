package pl.poweredbychocolate.auto.task;

import java.io.Closeable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.concurrent.TimeUnit;

public class TimerTaskScheduler implements Closeable {
  private final Timer timer = new Timer(true);
  private final List<DailyCheckBirthdayTask> scheduledThreadsList = new ArrayList<>();

  public void schedule(DailyCheckBirthdayTask dailyCheckBirthdayTask, Date scheduleTime) {
    scheduledThreadsList.add(dailyCheckBirthdayTask);
    long period = TimeUnit.MILLISECONDS.convert(1, TimeUnit.DAYS);
    timer.schedule(dailyCheckBirthdayTask, scheduleTime, period);
  }


  @Override
  public void close() {
    timer.cancel();
  }

  public void runThread() {
    scheduledThreadsList.forEach(DailyCheckBirthdayTask::run);
  }
}
