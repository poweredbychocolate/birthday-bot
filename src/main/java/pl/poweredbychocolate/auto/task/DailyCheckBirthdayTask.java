package pl.poweredbychocolate.auto.task;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import pl.poweredbychocolate.birthday.domain.Birthday;
import pl.poweredbychocolate.birthday.domain.BirthdayDate;
import pl.poweredbychocolate.birthday.repository.BirthdayRepositoryPort;
import pl.poweredbychocolate.discord.service.DiscordService;
import pl.poweredbychocolate.template.domain.Templates;
import pl.poweredbychocolate.template.service.ParseTemplateService;

import java.time.LocalDate;
import java.util.List;
import java.util.TimerTask;

@RequiredArgsConstructor
@Slf4j
public class DailyCheckBirthdayTask extends TimerTask {
  private final DiscordService discordService;
  private final BirthdayRepositoryPort birthdayRepositoryPort;
  private final ParseTemplateService parseTemplateService;
  private final Templates templates;

  @Override
  public void run() {
    log.debug("Start daily check birthday task");
    BirthdayDate queryBirthdayDate = queryCurrentDate();
    List<Birthday> celebratingBirthdayList = birthdayRepositoryPort.findBirthdayByBirthdayDateIfEnabled(queryBirthdayDate);

    if (!celebratingBirthdayList.isEmpty()) {
      String templateParsingResult = parseTemplate(celebratingBirthdayList);
      discordService.sendBirthdayMessage(templateParsingResult);
    }
  }

  private BirthdayDate queryCurrentDate() {
    LocalDate currentLocalDate = LocalDate.now();

    return BirthdayDate.builder()
        .day(currentLocalDate.getDayOfMonth())
        .month(currentLocalDate.getMonthValue())
        .build();
  }

  private String parseTemplate(List<Birthday> celebratingBirthdayList) {
    if (celebratingBirthdayList.size() == 1) {
      return parseTemplateService.parseTemplate(templates.getSingleTemplate(), celebratingBirthdayList.get(0));
    } else {
      return parseTemplateService.parseTemplate(templates.getMultipleTemplate(), celebratingBirthdayList);
    }
  }
}
