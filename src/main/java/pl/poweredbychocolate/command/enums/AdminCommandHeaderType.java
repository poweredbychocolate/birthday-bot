package pl.poweredbychocolate.command.enums;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public enum AdminCommandHeaderType {
  REMOVE_USER,
  START_BIRTHDAY_THREAD
}
