package pl.poweredbychocolate.command.enums;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public enum CommandHeaderType {
  USER_ADD,
  USER_REMOVE,
  USER_WHO,
  CALENDAR
}

