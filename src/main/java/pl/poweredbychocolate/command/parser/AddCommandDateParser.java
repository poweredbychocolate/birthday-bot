package pl.poweredbychocolate.command.parser;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import pl.poweredbychocolate.command.domain.CommandHeadersList;
import pl.poweredbychocolate.command.enums.CommandHeaderType;

import java.time.LocalDate;
import java.util.Optional;

@RequiredArgsConstructor
@Slf4j
public class AddCommandDateParser implements DateParser {
  private final CommandHeadersList commandHeadersList;

  private Integer day;
  private Integer month;
  private Integer year;

  @Override
  public Integer day() {
    return this.day;
  }

  @Override
  public Integer month() {
    return this.month;
  }

  @Override
  public Integer year() {
    return this.year;
  }

  @Override
  public boolean parse(String inputText) {
    String commandText = commandHeadersList.getCommandTextByType(CommandHeaderType.USER_ADD);
    String[] dateParts = inputText.replace(commandText, "")
        .strip()
        .split("\\.");

    boolean yearAvailable = dateParts.length == 3;

    Optional<LocalDate> parsedDate = datePartsToLocalDate(dateParts, yearAvailable);

    if (parsedDate.isPresent()) {
      this.day = parsedDate.get().getDayOfMonth();
      this.month = parsedDate.get().getMonthValue();
      this.year = yearAvailable ? parsedDate.get().getYear() : null;
      return true;
    }
    return false;
  }

  private Optional<LocalDate> datePartsToLocalDate(String[] dateParts, boolean yearAvailable) {
    try {
      return Optional.of(LocalDate.of(
          yearAvailable ? Integer.parseInt(dateParts[2]) : 1904,
          Integer.parseInt(dateParts[1]),
          Integer.parseInt(dateParts[0])));
    } catch (Exception exception) {
      log.debug("Date parts parse failed", exception);
      return Optional.empty();
    }
  }
}
