package pl.poweredbychocolate.command.parser;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import pl.poweredbychocolate.command.domain.CommandHeadersList;
import pl.poweredbychocolate.command.enums.CommandHeaderType;

import java.time.LocalDate;

@RequiredArgsConstructor
@Slf4j
public class WhoCommandDateParser implements DateParser {
  private final CommandHeadersList commandHeadersList;

  private Integer day;
  private Integer month;
  private Integer year;

  @Override
  public Integer day() {
    return this.day;
  }

  @Override
  public Integer month() {
    return this.month;
  }

  @Override
  public Integer year() {
    return this.year;
  }

  @Override
  public boolean parse(String inputText) {
    String commandText = commandHeadersList.getCommandTextByType(CommandHeaderType.USER_WHO);
    String[] dateParts = inputText.replace(commandText, "")
        .strip()
        .split("\\.");

    LocalDate parsedDate;
    try {
      if (dateParts.length == 1 && (dateParts[0].length() == 1 || dateParts[0].length() == 2)) {
        parsedDate = LocalDate.of(1904, Integer.parseInt(dateParts[0]), 1);
        this.month = parsedDate.getMonthValue();
        return true;
      }
      if (dateParts.length == 1 && dateParts[0].length() == 4) {
        parsedDate = LocalDate.of(Integer.parseInt(dateParts[0]), 1, 1);
        this.year = parsedDate.getYear();
        return true;
      } else if (dateParts.length == 3) {
        parsedDate = LocalDate.of(Integer.parseInt(dateParts[2]),
            Integer.parseInt(dateParts[1]),
            Integer.parseInt(dateParts[0]));
        this.day = parsedDate.getDayOfMonth();
        this.month = parsedDate.getMonthValue();
        this.year = parsedDate.getYear();
        return true;
      } else if (dateParts.length == 2 && dateParts[1].length() == 4) {
        parsedDate = LocalDate.of(Integer.parseInt(dateParts[1]),
            Integer.parseInt(dateParts[0]),
            1);
        this.month = parsedDate.getMonthValue();
        this.year = parsedDate.getYear();
        return true;
      } else if (dateParts.length == 2 && (dateParts[1].length() == 1 || dateParts[1].length() == 2)) {
        parsedDate = LocalDate.of(1904,
            Integer.parseInt(dateParts[1]),
            Integer.parseInt(dateParts[0]));
        this.day = parsedDate.getDayOfMonth();
        this.month = parsedDate.getMonthValue();
        return true;
      }
    } catch (Exception exception) {
      log.debug("Date parts parse failed", exception);
    }
    return false;
  }
}
