package pl.poweredbychocolate.command.parser;

public interface DateParser {
  Integer day();

  Integer month();

  Integer year();

  boolean parse(String inputText);
}
