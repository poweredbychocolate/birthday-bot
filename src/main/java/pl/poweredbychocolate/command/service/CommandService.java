package pl.poweredbychocolate.command.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import pl.poweredbychocolate.command.domain.*;
import pl.poweredbychocolate.command.enums.AdminCommandHeaderType;
import pl.poweredbychocolate.command.enums.CommandHeaderType;
import pl.poweredbychocolate.command.parser.AddCommandDateParser;
import pl.poweredbychocolate.command.parser.DateParser;
import pl.poweredbychocolate.command.parser.WhoCommandDateParser;

import java.util.Optional;

@RequiredArgsConstructor
@Slf4j
public class CommandService {
  private final CommandHeadersList commandHeadersList;
  private final AdminCommandHeadersList adminCommandHeadersList;

  public Optional<CommandHeaderType> parseCommandType(String inputText) {
    return commandHeadersList.matchCommandType(inputText);
  }

  public Optional<BirthdayAddCommand> parseUserAddCommand(String incomingMessageContent, Long userId, String userName) {
    DateParser dateParser = new AddCommandDateParser(commandHeadersList);

    if (dateParser.parse(incomingMessageContent)) {
      BirthdayAddCommand birthdayAddCommand = BirthdayAddCommand.builder()
          .userId(userId)
          .userName(userName)
          .day(dateParser.day())
          .month(dateParser.month())
          .year(dateParser.year())
          .build();
      return Optional.of(birthdayAddCommand);
    }
    return Optional.empty();
  }

  public BirthdayRemoveCommand parseUserRemoveCommand(Long userId, String userName) {
    return BirthdayRemoveCommand.builder()
        .userId(userId)
        .userName(userName)
        .build();
  }

  public Optional<BirthdayWhoCommand> parseUserWhoCommand(String incomingMessageContent) {
    DateParser dateParser = new WhoCommandDateParser(commandHeadersList);

    if (dateParser.parse(incomingMessageContent)) {
      BirthdayWhoCommand birthdayWhoCommand = BirthdayWhoCommand.builder()
          .day(dateParser.day())
          .month(dateParser.month())
          .year(dateParser.year())
          .build();
      return Optional.of(birthdayWhoCommand);
    }
    return Optional.empty();
  }

  public Optional<Long> parseAdminUserRemoveCommand(String incomingMessageContent) {
    String adminCommandText = adminCommandHeadersList.getCommandTextByType(AdminCommandHeaderType.REMOVE_USER);
    try {
      return Optional.of(Long.parseLong(incomingMessageContent.replace(adminCommandText, "").trim()));
    } catch (NumberFormatException e) {
      return Optional.empty();
    }
  }
}
