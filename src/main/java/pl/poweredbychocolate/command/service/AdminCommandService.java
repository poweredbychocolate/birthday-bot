package pl.poweredbychocolate.command.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import pl.poweredbychocolate.command.domain.AdminCommandHeadersList;
import pl.poweredbychocolate.command.enums.AdminCommandHeaderType;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Slf4j
public class AdminCommandService {

  private final AdminCommandHeadersList commandHeadersList;
  private final List<Long> allowedUsersIs;

  public Optional<AdminCommandHeaderType> parseCommandType(String inputText) {
    return commandHeadersList.matchCommandType(inputText);
  }

  public boolean hasPermission(long userId) {
    return allowedUsersIs.contains(userId);
  }
}
