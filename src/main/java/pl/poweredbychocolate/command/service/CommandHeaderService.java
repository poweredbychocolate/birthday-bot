package pl.poweredbychocolate.command.service;

import lombok.RequiredArgsConstructor;
import pl.poweredbychocolate.command.builder.AdminCommandHeadersListBuilder;
import pl.poweredbychocolate.command.builder.CommandHeadersListBuilder;
import pl.poweredbychocolate.command.domain.AdminCommandHeadersList;
import pl.poweredbychocolate.command.domain.CommandHeadersList;
import pl.poweredbychocolate.command.enums.AdminCommandHeaderType;
import pl.poweredbychocolate.command.enums.CommandHeaderType;

import java.util.ResourceBundle;

@RequiredArgsConstructor
public class CommandHeaderService {
  private final ResourceBundle userResourceBundle;

  public CommandHeadersList buildCommandHeadersList() {
    return CommandHeadersListBuilder.builder()
        .command(userResourceBundle.getString("command.birthday.add"), CommandHeaderType.USER_ADD)
        .command(userResourceBundle.getString("command.birthday.who"), CommandHeaderType.USER_WHO)
        .command(userResourceBundle.getString("command.birthday.remove"), CommandHeaderType.USER_REMOVE)
        .command(userResourceBundle.getString("command.calendar"), CommandHeaderType.CALENDAR)
        .build();
  }

  public AdminCommandHeadersList buildAdminCommandHeadersList() {
  return AdminCommandHeadersListBuilder.builder()
      .command(userResourceBundle.getString("command.admin.birthday.thread.start"), AdminCommandHeaderType.START_BIRTHDAY_THREAD)
      .command(userResourceBundle.getString("command.admin.remove.user"), AdminCommandHeaderType.REMOVE_USER)
      .build();
  }
}
