package pl.poweredbychocolate.command.builder;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import pl.poweredbychocolate.command.domain.AdminCommandHeadersList;
import pl.poweredbychocolate.command.enums.AdminCommandHeaderType;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class AdminCommandHeadersListBuilder {
  private final AdminCommandHeadersList commandHeadersList = new AdminCommandHeadersList();

  public static AdminCommandHeadersListBuilder builder() {
    return new AdminCommandHeadersListBuilder();
  }

  public AdminCommandHeadersListBuilder command(String commandText, AdminCommandHeaderType adminCommandHeaderType) {
    this.commandHeadersList.applyCommand(commandText, adminCommandHeaderType);
    return this;
  }

  public AdminCommandHeadersList build() {
    return this.commandHeadersList;
  }

}
