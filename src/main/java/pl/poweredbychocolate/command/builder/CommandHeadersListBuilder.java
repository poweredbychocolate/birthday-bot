package pl.poweredbychocolate.command.builder;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import pl.poweredbychocolate.command.domain.CommandHeadersList;
import pl.poweredbychocolate.command.enums.CommandHeaderType;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class CommandHeadersListBuilder {
  private final CommandHeadersList commandHeadersList = new CommandHeadersList();

  public static CommandHeadersListBuilder builder() {
    return new CommandHeadersListBuilder();
  }

  public CommandHeadersListBuilder command(String commandText, CommandHeaderType commandHeaderType) {
    this.commandHeadersList.applyCommand(commandText, commandHeaderType);
    return this;
  }

  public CommandHeadersList build() {
    return this.commandHeadersList;
  }

}
