package pl.poweredbychocolate.command.domain;

import lombok.NoArgsConstructor;
import pl.poweredbychocolate.command.enums.AdminCommandHeaderType;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@NoArgsConstructor
public class AdminCommandHeadersList {
  private final List<AdminCommandHeader> commandHeaders = new ArrayList<>();

  public void applyCommand(String commandText, AdminCommandHeaderType adminCommandHeaderType) {
    commandHeaders.add(AdminCommandHeader.builder()
        .type(adminCommandHeaderType)
        .command(commandText)
        .build());
  }

  public Optional<AdminCommandHeaderType> matchCommandType(String inputText) {
    return commandHeaders.stream()
        .filter(commandHeader -> commandHeader.isMatch(inputText))
        .map(AdminCommandHeader::getType)
        .findFirst();
  }

  public String getCommandTextByType(AdminCommandHeaderType adminCommandHeaderType) {
    return commandHeaders.stream()
        .filter(commandHeader -> commandHeader.getType().equals(adminCommandHeaderType))
        .findFirst()
        .map(AdminCommandHeader::getCommand)
        .orElseThrow();
  }
}
