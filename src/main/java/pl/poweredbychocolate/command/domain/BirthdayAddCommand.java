package pl.poweredbychocolate.command.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
public class BirthdayAddCommand {
  private Integer day;
  private Integer month;
  private Integer year;
  private Long userId;
  private String userName;
}
