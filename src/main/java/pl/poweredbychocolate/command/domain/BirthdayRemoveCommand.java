package pl.poweredbychocolate.command.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
public class BirthdayRemoveCommand {
  private Long userId;
  private String userName;
}
