package pl.poweredbychocolate.command.domain;

import lombok.NoArgsConstructor;
import pl.poweredbychocolate.command.enums.CommandHeaderType;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@NoArgsConstructor
public class CommandHeadersList {
  private final List<CommandHeader> commandHeaders = new ArrayList<>();

  public void applyCommand(String commandText, CommandHeaderType commandHeaderType) {
    commandHeaders.add(CommandHeader.builder()
        .type(commandHeaderType)
        .command(commandText)
        .build());
  }

  public Optional<CommandHeaderType> matchCommandType(String inputText) {
    return commandHeaders.stream()
        .filter(commandHeader -> commandHeader.isMatch(inputText))
        .map(CommandHeader::getType)
        .findFirst();
  }

  public String getCommandTextByType(CommandHeaderType commandHeaderType) {
    return commandHeaders.stream()
        .filter(commandHeader -> commandHeader.getType().equals(commandHeaderType))
        .findFirst()
        .map(CommandHeader::getCommand)
        .orElseThrow();
  }
}
