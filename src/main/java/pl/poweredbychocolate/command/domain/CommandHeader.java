package pl.poweredbychocolate.command.domain;

import lombok.Builder;
import lombok.Getter;
import lombok.Value;
import pl.poweredbychocolate.command.enums.CommandHeaderType;

@Builder
@Value
public class CommandHeader {
  @Getter
  CommandHeaderType type;
  @Getter
  String command;

  boolean isMatch(String inputCommand) {
    return inputCommand.startsWith(command);
  }
}
