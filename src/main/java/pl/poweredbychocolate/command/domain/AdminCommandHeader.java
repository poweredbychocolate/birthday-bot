package pl.poweredbychocolate.command.domain;

import lombok.Builder;
import lombok.Getter;
import lombok.Value;
import pl.poweredbychocolate.command.enums.AdminCommandHeaderType;

@Builder
@Value
public class AdminCommandHeader {
  @Getter
  AdminCommandHeaderType type;
  @Getter
  String command;

  boolean isMatch(String inputCommand) {
    return inputCommand.startsWith(command);
  }
}
