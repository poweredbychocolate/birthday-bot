package pl.poweredbychocolate.config.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import pl.poweredbychocolate.config.enums.TemplateName;
import pl.poweredbychocolate.storage.StorageService;
import pl.poweredbychocolate.storage.domain.ConfigFile;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;
import java.util.ResourceBundle;

@RequiredArgsConstructor
@Slf4j
public class GenerateConfigService {
  private final StorageService storageService;
  private final ResourceBundle userResourceBundle;

  public void generate() throws IOException {
    log.info(userResourceBundle.getString("generating.config"));
    generateAndSaveAppConfig();
    log.info(userResourceBundle.getString("generating.template.single"));
    generateAndSaveSingleUserTemplate();
    log.info(userResourceBundle.getString("generating.template.multi"));
    generateAndSaveMultipleUserTemplate();
    log.info(userResourceBundle.getString("done"));
  }

  private void generateAndSaveAppConfig() throws IOException {
    ConfigFile configFileTemplate = ConfigFile.builder()
        .botToken("bot-token-here")
        .notificationChannelId(12345678987L)
        .birthdayCheckHour(8)
        .birthdayCheckMinute(0)
        .adminUserIds(List.of(10L, 20L, 30L))
        .build();

    storageService.saveConfigFile(configFileTemplate);
  }

  private void generateAndSaveSingleUserTemplate() throws IOException {
    var singleUserTemplate = "Today ${user.name} celebrates his ${if.age} ${user.age} ${if.age} birthday. Happy Birthday";
    Path singleUserTemplatePath = TemplateName.SINGLE_USER_TEMPLATE.path();

    storageService.saveUserTemplate(singleUserTemplate, singleUserTemplatePath);
  }

  private void generateAndSaveMultipleUserTemplate() throws IOException {
    var multipleUserTemplate = "Today ${loop} ${user.name} celebrates his ${if.age} ${user.age} ${if.age} birthday ${loop}. Happy Birthday for all";
    Path multipleUserTemplatePath = TemplateName.MULTI_USER_TEMPLATE.path();

    storageService.saveUserTemplate(multipleUserTemplate, multipleUserTemplatePath);
  }
}
