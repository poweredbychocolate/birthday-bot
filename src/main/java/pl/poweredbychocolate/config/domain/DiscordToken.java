package pl.poweredbychocolate.config.domain;

import pl.poweredbychocolate.config.exception.InvalidDiscordTokenException;

import java.util.Objects;

public class DiscordToken {
  private final String tokenString;

  public DiscordToken(String tokenString) {
    if (isNotValid(tokenString)) {
      throw new InvalidDiscordTokenException(tokenString);
    }
    this.tokenString = tokenString;
  }

  public static DiscordToken of(String tokenString) {
    return new DiscordToken(tokenString);
  }

  public String fetch() {
    return tokenString;
  }

  private boolean isNotValid(String tokenString) {
    return Objects.isNull(tokenString)
        || tokenString.isEmpty();
  }
}