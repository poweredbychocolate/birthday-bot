package pl.poweredbychocolate.config.domain;

import pl.poweredbychocolate.config.exception.InvalidDiscordChannelIdException;

import java.util.Objects;

public class DiscordChannelId {
  private final Long channelId;

  public DiscordChannelId(Long channelId) {
    if (isNotValid(channelId)) {
      throw new InvalidDiscordChannelIdException(channelId);
    }
    this.channelId = channelId;
  }

  public static DiscordChannelId of(Long channelId) {
    return new DiscordChannelId(channelId);
  }

  public Long fetch() {
    return channelId;
  }

  public boolean match(Long inputChannelId) {
    return this.channelId.equals(inputChannelId);
  }

  private boolean isNotValid(Long channelId) {
    return Objects.isNull(channelId);
  }
}
