package pl.poweredbychocolate.config.exception;

public class InvalidDiscordChannelIdException extends RuntimeException {
  public InvalidDiscordChannelIdException(Long channelId) {
    super(String.format("Invalid discord channel id %d exception", channelId));
  }
}

