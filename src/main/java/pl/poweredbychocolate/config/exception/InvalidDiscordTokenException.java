package pl.poweredbychocolate.config.exception;

public class InvalidDiscordTokenException extends RuntimeException {
  public InvalidDiscordTokenException(String tokenString) {
    super(String.format("Invalid discord token %s exception", tokenString));
  }
}

