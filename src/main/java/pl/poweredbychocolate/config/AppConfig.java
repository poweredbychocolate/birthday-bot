package pl.poweredbychocolate.config;

import lombok.Builder;
import pl.poweredbychocolate.config.domain.DiscordChannelId;
import pl.poweredbychocolate.config.domain.DiscordToken;
import pl.poweredbychocolate.storage.domain.ConfigFile;

import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Builder
public class AppConfig {
  private final DiscordToken botToken;
  private final DiscordChannelId notificationChannelId;
  private final Date dailyCheckBirthdayDate;
  private final List<Long> userIdsWithAdminPermissionList;

  public static AppConfig of(ConfigFile configFile) {
    Calendar scheduleTime = configTimeToCalendar(configFile);

    return AppConfig.builder()
        .botToken(new DiscordToken(configFile.getBotToken()))
        .notificationChannelId(new DiscordChannelId(configFile.getNotificationChannelId()))
        .dailyCheckBirthdayDate(scheduleTime.getTime())
        .userIdsWithAdminPermissionList(configFile.getAdminUserIds())
        .build();
  }

  private static Calendar configTimeToCalendar(ConfigFile configFile) {
    Calendar scheduleTime = Calendar.getInstance();
    scheduleTime.set(Calendar.HOUR_OF_DAY, configFile.getBirthdayCheckHour());
    scheduleTime.set(Calendar.MINUTE, configFile.getBirthdayCheckMinute());
    scheduleTime.set(Calendar.SECOND, 0);

    if (isCurrentDateGreaterThenConfigDate(configFile, LocalDateTime.now())) {
      scheduleTime.set(Calendar.DAY_OF_MONTH, scheduleTime.get(Calendar.DAY_OF_MONTH) + 1);
    }
    return scheduleTime;
  }

  private static boolean isCurrentDateGreaterThenConfigDate(ConfigFile configFile, LocalDateTime now) {
    return configFile.getBirthdayCheckHour() < now.getHour()
        || configFile.getBirthdayCheckHour() == now.getHour()
        && configFile.getBirthdayCheckMinute() < now.getMinute();
  }

  public String token() {
    return botToken.fetch();
  }

  public Long notificationChannel() {
    return notificationChannelId.fetch();
  }

  public Date dailyCheckBirthdayDate() {
    return dailyCheckBirthdayDate;
  }

  public List<Long> getUserIdsWithAdminPermissionList() {
    return userIdsWithAdminPermissionList;
  }
}
