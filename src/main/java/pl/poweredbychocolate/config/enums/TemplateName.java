package pl.poweredbychocolate.config.enums;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.nio.file.Path;

@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public enum TemplateName {

  SINGLE_USER_TEMPLATE("single-user-template.txt"),
  MULTI_USER_TEMPLATE("multi-user-template.txt");

  @Getter
  private final String name;

  public Path path(){
    return Path.of(name);
  }
}
