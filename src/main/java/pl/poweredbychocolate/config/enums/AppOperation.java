package pl.poweredbychocolate.config.enums;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public enum AppOperation {
  GENERATE_CONFIG("gen-config");
  @Getter
  private final String operation;
}
