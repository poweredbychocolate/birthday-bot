package pl.poweredbychocolate.discord.service;

import lombok.extern.slf4j.Slf4j;
import org.javacord.api.DiscordApi;
import org.javacord.api.DiscordApiBuilder;
import org.javacord.api.entity.channel.Channel;
import org.javacord.api.entity.channel.ServerTextChannel;
import org.javacord.api.entity.intent.Intent;
import org.javacord.api.entity.message.MessageBuilder;
import pl.poweredbychocolate.config.AppConfig;
import pl.poweredbychocolate.discord.exception.DiscordLoginFailException;
import pl.poweredbychocolate.discord.listener.DiscordServiceListenersPack;

import java.io.Closeable;

@Slf4j
public class DiscordService implements Closeable {
  private final AppConfig appConfig;
  private final DiscordApi discordApi;
  private final ServerTextChannel notificationChannel;


  public DiscordService(AppConfig appConfig, DiscordServiceListenersPack discordServiceListenersPack) throws DiscordLoginFailException {
    this.appConfig = appConfig;
    try {
      Intent[] intents = {
          Intent.GUILDS,
          Intent.GUILD_MEMBERS,
          Intent.GUILD_MESSAGES,
          Intent.GUILD_MESSAGE_TYPING,
          Intent.GUILD_MESSAGE_REACTIONS,
          Intent.DIRECT_MESSAGES,
          Intent.DIRECT_MESSAGE_TYPING,
          Intent.DIRECT_MESSAGE_REACTIONS,
          Intent.MESSAGE_CONTENT
      };

      discordApi = new DiscordApiBuilder()
          .setToken(appConfig.token())
          .setIntents(intents)
          .login()
          .join();


    } catch (Exception e) {
      log.debug("Cannot connect to discord api ", e);
      throw new DiscordLoginFailException();
    }

    discordServiceListenersPack.appendListeners(discordApi);
    this.notificationChannel = findNotificationChannelId();
  }

  private ServerTextChannel findNotificationChannelId() {
    return discordApi.getChannelById(appConfig.notificationChannel())
        .flatMap(Channel::asServerTextChannel)
        .orElseThrow();
  }

  @Override
  public void close() {
    discordApi.disconnect();
  }

  public void sendBirthdayMessage(String templateParsingResult) {
    MessageBuilder messageBuilder = new MessageBuilder()
        .append(templateParsingResult);

    messageBuilder.send(notificationChannel);
  }
}
