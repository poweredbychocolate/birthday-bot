package pl.poweredbychocolate.discord.service;

import lombok.RequiredArgsConstructor;
import org.javacord.api.entity.message.MessageBuilder;
import org.javacord.api.entity.message.MessageDecoration;
import org.javacord.api.entity.message.embed.EmbedBuilder;
import pl.poweredbychocolate.birthday.domain.Birthday;

import java.awt.*;
import java.io.InputStream;
import java.util.List;
import java.util.ResourceBundle;

@RequiredArgsConstructor
public class CommandStateResponseService {

  private final ResourceBundle userResourceBundle;

  public MessageBuilder commandIncorrectDate() {
    return new MessageBuilder()
        .setEmbed(embedMessage(userResourceBundle.getString("command.incorrect.date"), Color.RED));
  }

  public MessageBuilder whoCommandEmptyUsersList() {
    return new MessageBuilder()
        .setEmbed(embedMessage(userResourceBundle.getString("command.who.empty"), Color.ORANGE));
  }

  public MessageBuilder whoCommandUsersListResponse(List<Birthday> birthday) {
    StringBuilder builder = new StringBuilder();
    birthday.forEach(birthdayUser -> builder.append(birthdayUser.getUserNicknameOrName()).append(System.lineSeparator()));

    return new MessageBuilder()
        .append(userResourceBundle.getString("command.who.list.header"), MessageDecoration.BOLD)
        .append(System.lineSeparator())
        .append(builder.toString());
  }

  public MessageBuilder commandAddSaved() {
    return new MessageBuilder()
        .setEmbed(embedMessage(userResourceBundle.getString("command.add.saved"), Color.GREEN));
  }

  private EmbedBuilder embedMessage(String title, Color color) {
    return new EmbedBuilder()
        .setTitle(title)
        .setColor(color);
  }

  public MessageBuilder commandAddReplaced() {
    return new MessageBuilder()
        .setEmbed(embedMessage(userResourceBundle.getString("command.add.replace"), Color.BLUE));
  }

  public MessageBuilder commandRemoveNoUser() {
    return new MessageBuilder()
        .setEmbed(embedMessage(userResourceBundle.getString("command.remove.no.user"), Color.RED));
  }

  public MessageBuilder commandRemoveSuccessfully() {
    return new MessageBuilder()
        .setEmbed(embedMessage(userResourceBundle.getString("command.remove.successfully"), Color.GREEN));
  }

  public MessageBuilder unexpectedError() {
    return new MessageBuilder()
        .setEmbed(embedMessage(userResourceBundle.getString("command.unexpected.error"), Color.MAGENTA));
  }

  public MessageBuilder adminThreadStart() {
    return new MessageBuilder()
        .setEmbed(embedMessage(userResourceBundle.getString("command.admin.thread.start"), Color.GREEN));
  }

  public MessageBuilder adminCommandIncorrectUserId() {
    return new MessageBuilder()
        .setEmbed(embedMessage(userResourceBundle.getString("command.admin.remove.incorrect.user.id"), Color.RED));
  }

  public MessageBuilder adminCommandUserRemoved() {
    return new MessageBuilder()
        .setEmbed(embedMessage(userResourceBundle.getString("command.admin.remove.incorrect.successfully"), Color.GREEN));
  }

  public MessageBuilder sendCalendarAsAttachment(String calendarName, InputStream calendarInputStream) {
    return new MessageBuilder()
        .setEmbed(embedMessage(userResourceBundle.getString("generating.calendar.finish"), Color.GREEN))
        .addAttachment(calendarInputStream, calendarName);
  }

  public MessageBuilder startGeneratingCalendar() {
    return new MessageBuilder()
        .setEmbed(embedMessage(userResourceBundle.getString("generating.calendar.start"), Color.ORANGE));
  }

  public MessageBuilder generatingCalendarError() {
    return new MessageBuilder()
        .setEmbed(embedMessage(userResourceBundle.getString("generating.calendar.error"), Color.RED));
  }
}
