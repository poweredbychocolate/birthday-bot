package pl.poweredbychocolate.discord.mapper;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import pl.poweredbychocolate.birthday.domain.Birthday;
import pl.poweredbychocolate.birthday.domain.BirthdayDate;
import pl.poweredbychocolate.birthday.domain.BirthdayUser;
import pl.poweredbychocolate.command.domain.BirthdayAddCommand;
import pl.poweredbychocolate.command.domain.BirthdayWhoCommand;


@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class CommandMapper {

  public static Birthday toBirthday(BirthdayAddCommand birthdayAddCommand) {
    BirthdayUser birthdayUser = BirthdayUser.builder()
        .userId(birthdayAddCommand.getUserId())
        .userName(birthdayAddCommand.getUserName())
        .enabled(true)
        .build();

    BirthdayDate birthdayDate = BirthdayDate.builder()
        .day(birthdayAddCommand.getDay())
        .month(birthdayAddCommand.getMonth())
        .year(birthdayAddCommand.getYear())
        .build();

    return Birthday.builder()
        .user(birthdayUser)
        .date(birthdayDate)
        .build();
  }

  public static BirthdayDate toBirthdayDate(BirthdayWhoCommand birthdayWhoCommand) {
    return BirthdayDate.builder()
        .day(birthdayWhoCommand.getDay())
        .month(birthdayWhoCommand.getMonth())
        .year(birthdayWhoCommand.getYear())
        .build();
  }
}
