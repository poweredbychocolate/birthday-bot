package pl.poweredbychocolate.discord.exception;

public class DiscordLoginFailException extends Exception{
  public DiscordLoginFailException() {
    super("Cannot connect to discord api");
  }
}
