package pl.poweredbychocolate.discord.listener;

import lombok.Builder;
import lombok.Value;
import org.javacord.api.DiscordApi;

@Value
@Builder
public class DiscordServiceListenersPack {
  BotCommandListener botCommandListener;
  UsersNicknameListener usersNicknameListener;
  UsersNameListener usersNameListener;
  UsersLeaveServerListener usersLeaveListener;
  UsersJoinToServerListener usersJoinToServerListener;

  public void appendListeners(DiscordApi discordApi) {

      discordApi.addMessageCreateListener(botCommandListener);
      discordApi.addUserChangeNicknameListener(usersNicknameListener);
      discordApi.addUserChangeNameListener(usersNameListener);
      discordApi.addServerMemberLeaveListener(usersLeaveListener);
      discordApi.addServerMemberJoinListener(usersJoinToServerListener);

  }
}
