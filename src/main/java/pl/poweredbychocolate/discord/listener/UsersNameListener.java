package pl.poweredbychocolate.discord.listener;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.javacord.api.event.user.UserChangeNameEvent;
import org.javacord.api.listener.user.UserChangeNameListener;
import pl.poweredbychocolate.birthday.domain.Birthday;
import pl.poweredbychocolate.birthday.repository.BirthdayRepositoryPort;

import java.util.Optional;

@RequiredArgsConstructor
@Slf4j
public class UsersNameListener implements UserChangeNameListener {
  private final BirthdayRepositoryPort birthdayRepositoryPort;

  @Override
  public void onUserChangeName(UserChangeNameEvent userChangeNameEvent) {
    log.debug("User {} change name", userChangeNameEvent.getUser().getName());
    long userId = userChangeNameEvent.getUser().getId();
    Optional<Birthday> birthdayUser = birthdayRepositoryPort.findById(userId);

    birthdayUser.ifPresent(birthday -> {
      String userNewName = userChangeNameEvent.getNewName();
      changeUserName(birthday, userNewName);
    });
  }

  private void changeUserName(Birthday currentBirthday, String newUserName) {
    try {
      currentBirthday.setUserName(newUserName);
      birthdayRepositoryPort.replace(currentBirthday);
    } catch (Exception changeUserNameException) {
      log.warn("User update name fail");
      log.debug("Exception during update user name", changeUserNameException);
    }
  }
}