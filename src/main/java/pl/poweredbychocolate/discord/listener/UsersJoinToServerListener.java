package pl.poweredbychocolate.discord.listener;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.javacord.api.event.server.member.ServerMemberJoinEvent;
import org.javacord.api.listener.server.member.ServerMemberJoinListener;
import pl.poweredbychocolate.birthday.domain.Birthday;
import pl.poweredbychocolate.birthday.repository.BirthdayRepositoryPort;

import java.util.Optional;

@RequiredArgsConstructor
@Slf4j
public class UsersJoinToServerListener implements ServerMemberJoinListener {
  private final BirthdayRepositoryPort birthdayRepositoryPort;

  @Override
  public void onServerMemberJoin(ServerMemberJoinEvent serverMemberJoinEvent) {
    log.debug("User {} join to server", serverMemberJoinEvent.getUser().getName());
    long userId = serverMemberJoinEvent.getUser().getId();
    Optional<Birthday> birthdayUser = birthdayRepositoryPort.findById(userId);

    birthdayUser.ifPresent(birthday -> {
      birthday.enable();

      try {
        birthdayRepositoryPort.replace(birthday);
      } catch (Exception enableUserException) {
        log.warn("User update on join to server fail");
        log.debug("Exception during enable user birthday", enableUserException);
      }
    });
  }
}