package pl.poweredbychocolate.discord.listener;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.javacord.api.event.server.member.ServerMemberLeaveEvent;
import org.javacord.api.listener.server.member.ServerMemberLeaveListener;
import pl.poweredbychocolate.birthday.domain.Birthday;
import pl.poweredbychocolate.birthday.repository.BirthdayRepositoryPort;

import java.util.Optional;

@RequiredArgsConstructor
@Slf4j
public class UsersLeaveServerListener implements ServerMemberLeaveListener {
  private final BirthdayRepositoryPort birthdayRepositoryPort;

  @Override
  public void onServerMemberLeave(ServerMemberLeaveEvent serverMemberLeaveEvent) {
    log.debug("User {} leave server", serverMemberLeaveEvent.getUser().getName());
    long userId = serverMemberLeaveEvent.getUser().getId();
    Optional<Birthday> birthdayUser = birthdayRepositoryPort.findById(userId);

    birthdayUser.ifPresent(birthday -> {
      birthday.disable();

      try {
        birthdayRepositoryPort.replace(birthday);
      } catch (Exception disableUserException) {
        log.warn("User update on leave server fail");
        log.debug("Exception during disable user birthday", disableUserException);
      }
    });

  }
}