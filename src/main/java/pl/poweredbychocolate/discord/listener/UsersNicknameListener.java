package pl.poweredbychocolate.discord.listener;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.javacord.api.event.user.UserChangeNicknameEvent;
import org.javacord.api.listener.user.UserChangeNicknameListener;
import pl.poweredbychocolate.birthday.domain.Birthday;
import pl.poweredbychocolate.birthday.repository.BirthdayRepositoryPort;

import java.util.Optional;

@RequiredArgsConstructor
@Slf4j
public class UsersNicknameListener implements UserChangeNicknameListener {
  private final BirthdayRepositoryPort birthdayRepositoryPort;

  @Override
  public void onUserChangeNickname(UserChangeNicknameEvent userChangeNicknameEvent) {
    log.debug("User {} change nickname", userChangeNicknameEvent.getUser().getName());
    long userId = userChangeNicknameEvent.getUser().getId();
    Optional<Birthday> birthdayUser = birthdayRepositoryPort.findById(userId);

    birthdayUser.ifPresent(birthday -> {
      Optional<String> userNewNickname = userChangeNicknameEvent.getNewNickname();
      userNewNickname.ifPresent(newNickname -> changeUserNickname(birthday, newNickname));
    });
  }

  private void changeUserNickname(Birthday currentBirthday, String newUserNickname) {
    try {
      currentBirthday.setUserNickname(newUserNickname);
      birthdayRepositoryPort.replace(currentBirthday);
    } catch (Exception changeUserNicknameException) {
      log.warn("User update nickname fail");
      log.debug("Exception during update user nickname", changeUserNicknameException);
    }
  }
}
