package pl.poweredbychocolate.discord.listener;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.javacord.api.entity.channel.TextChannel;
import org.javacord.api.entity.message.MessageAuthor;
import org.javacord.api.entity.server.Server;
import org.javacord.api.entity.user.User;
import org.javacord.api.event.message.MessageCreateEvent;
import org.javacord.api.listener.message.MessageCreateListener;
import pl.poweredbychocolate.auto.task.TimerTaskScheduler;
import pl.poweredbychocolate.birthday.domain.Birthday;
import pl.poweredbychocolate.birthday.domain.BirthdayUser;
import pl.poweredbychocolate.birthday.repository.BirthdayRepositoryPort;
import pl.poweredbychocolate.calendar.domain.CalendarUser;
import pl.poweredbychocolate.calendar.service.CalendarBuildService;
import pl.poweredbychocolate.command.domain.BirthdayAddCommand;
import pl.poweredbychocolate.command.domain.BirthdayRemoveCommand;
import pl.poweredbychocolate.command.domain.BirthdayWhoCommand;
import pl.poweredbychocolate.command.enums.AdminCommandHeaderType;
import pl.poweredbychocolate.command.enums.CommandHeaderType;
import pl.poweredbychocolate.command.service.AdminCommandService;
import pl.poweredbychocolate.command.service.CommandService;
import pl.poweredbychocolate.discord.mapper.CommandMapper;
import pl.poweredbychocolate.discord.service.CommandStateResponseService;

import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.concurrent.CancellationException;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;
import java.util.stream.Collectors;

@Slf4j
@RequiredArgsConstructor
public class BotCommandListener implements MessageCreateListener {
  private final CommandService commandParseService;
  private final BirthdayRepositoryPort birthdayRepositoryPort;
  private final CommandStateResponseService commandStateResponseService;
  private final AdminCommandService adminCommandService;
  private final TimerTaskScheduler timerTaskScheduler;
  private final CalendarBuildService calendarBuildService;
  private final ResourceBundle userResourceBundle;

  @Override
  public void onMessageCreate(MessageCreateEvent messageCreateEvent) {
    if (isServerUserMessage(messageCreateEvent)) {
      handleServerMessage(messageCreateEvent);
    } else if (isPrivateAdminServerMessage(messageCreateEvent)) {
      handlePrivateServerAdminMessage(messageCreateEvent);
    }
  }

  private void handleServerMessage(MessageCreateEvent messageCreateEvent) {
    String incomingMessageContent = messageCreateEvent.getMessageContent();
    Server messageServer = messageCreateEvent.getServer().orElseThrow();
    Optional<CommandHeaderType> commandHeaderType = commandParseService.parseCommandType(incomingMessageContent);
    log.info(incomingMessageContent);

    commandHeaderType.ifPresent(commandType -> {
      log.info(commandType.toString());

      MessageAuthor messageAuthor = messageCreateEvent.getMessageAuthor();
      TextChannel eventTextChannel = messageCreateEvent.getChannel();
      handleCommand(commandType, incomingMessageContent, messageAuthor, eventTextChannel, messageServer);
    });
  }

  private void handlePrivateServerAdminMessage(MessageCreateEvent messageCreateEvent) {
    String incomingMessageContent = messageCreateEvent.getMessageContent();
    Optional<AdminCommandHeaderType> commandHeaderType = adminCommandService.parseCommandType(incomingMessageContent);
    log.info(incomingMessageContent);

    commandHeaderType.ifPresent(adminCommandHeaderType -> {
      log.info(adminCommandHeaderType.toString());

      TextChannel adminEventTextChannel = messageCreateEvent.getChannel();
      handleAdminCommand(incomingMessageContent, adminCommandHeaderType, adminEventTextChannel);
    });
  }

  private void handleCommand(CommandHeaderType commandType, String incomingMessageContent, MessageAuthor messageAuthor, TextChannel eventTextChannel, Server messageServer) {
    switch (commandType) {
      case USER_ADD:
        handleUserAddCommand(incomingMessageContent, messageAuthor, eventTextChannel);
        break;
      case USER_REMOVE:
        handleUserRemoveCommand(messageAuthor, eventTextChannel);
        break;
      case USER_WHO:
        handleUserWhoCommand(incomingMessageContent, eventTextChannel);
        break;
      case CALENDAR:
        handleCalendarCommand(eventTextChannel, messageServer);
        break;
    }
  }

  private void handleAdminCommand(String incomingMessageContent, AdminCommandHeaderType adminCommandHeaderType, TextChannel adminEventTextChannel) {
    if (adminCommandHeaderType == AdminCommandHeaderType.START_BIRTHDAY_THREAD) {
      handleAdminBirthdayThreadStart(adminEventTextChannel);
    } else if (adminCommandHeaderType == AdminCommandHeaderType.REMOVE_USER) {
      handleAdminUserRemove(incomingMessageContent, adminEventTextChannel);
    }
  }


  private void handleAdminBirthdayThreadStart(TextChannel adminEventTextChannel) {
    timerTaskScheduler.runThread();
    commandStateResponseService.adminThreadStart().send(adminEventTextChannel);
  }

  private void handleAdminUserRemove(String incomingMessageContent, TextChannel adminEventTextChannel) {
    Optional<Long> userToRemoveId = commandParseService.parseAdminUserRemoveCommand(incomingMessageContent);

    if (userToRemoveId.isPresent()) {
      try {
        birthdayRepositoryPort.deleteById(userToRemoveId.get());
        commandStateResponseService.adminCommandUserRemoved().send(adminEventTextChannel);
      } catch (IOException ioException) {
        commandStateResponseService.unexpectedError().send(adminEventTextChannel);
        log.warn("Handle user remove command exception", ioException);
      }
    } else {
      commandStateResponseService.adminCommandIncorrectUserId().send(adminEventTextChannel);
    }
  }

  private void handleUserWhoCommand(String incomingMessageContent, TextChannel eventTextChannel) {
    Optional<BirthdayWhoCommand> birthdayWhoCommand = commandParseService.parseUserWhoCommand(incomingMessageContent);

    if (birthdayWhoCommand.isPresent()) {
      List<Birthday> birthdayList = birthdayRepositoryPort.findBirthdayByBirthdayDateIfEnabled(CommandMapper.toBirthdayDate(birthdayWhoCommand.get()));
      if (birthdayList.isEmpty()) {
        commandStateResponseService.whoCommandEmptyUsersList().send(eventTextChannel);
      } else {
        commandStateResponseService.whoCommandUsersListResponse(birthdayList).send(eventTextChannel);
      }
    } else {
      commandStateResponseService.commandIncorrectDate().send(eventTextChannel);
    }
  }

  private void handleUserRemoveCommand(MessageAuthor messageAuthor, TextChannel eventTextChannel) {
    BirthdayRemoveCommand birthdayRemoveCommand = commandParseService.parseUserRemoveCommand(messageAuthor.getId(), messageAuthor.getName());
    Optional<BirthdayUser> birthdayUserToRemove = birthdayRepositoryPort.findById(birthdayRemoveCommand.getUserId())
        .map(Birthday::getUser);

    if (birthdayUserToRemove.isPresent()) {
      try {
        birthdayRepositoryPort.delete(birthdayUserToRemove.get());
        commandStateResponseService.commandRemoveSuccessfully().send(eventTextChannel);
      } catch (IOException ioException) {
        commandStateResponseService.unexpectedError().send(eventTextChannel);
        log.warn("Handle user remove command exception", ioException);
      }
    } else {
      commandStateResponseService.commandRemoveNoUser().send(eventTextChannel);
    }
  }

  private void handleUserAddCommand(String incomingMessageContent, MessageAuthor messageAuthor, TextChannel eventTextChannel) {
    Optional<BirthdayAddCommand> birthdayAddCommand = commandParseService.parseUserAddCommand(incomingMessageContent, messageAuthor.getId(), messageAuthor.getName());
    if (birthdayAddCommand.isPresent()) {

      Optional<Birthday> birthday = birthdayRepositoryPort.findById(birthdayAddCommand.get().getUserId());
      if (birthday.isEmpty()) {
        try {
          birthdayRepositoryPort.save(CommandMapper.toBirthday(birthdayAddCommand.get()));
          commandStateResponseService.commandAddSaved().send(eventTextChannel);
        } catch (IOException ioException) {
          commandStateResponseService.unexpectedError().send(eventTextChannel);
          log.warn("Handle user add command save exception", ioException);
        }
      } else {
        try {
          birthdayRepositoryPort.replace(CommandMapper.toBirthday(birthdayAddCommand.get()));
          commandStateResponseService.commandAddReplaced().send(eventTextChannel);
        } catch (IOException ioException) {
          commandStateResponseService.unexpectedError().send(eventTextChannel);
          log.warn("Handle user add command replace exception", ioException);
        }
      }
    } else {
      commandStateResponseService.commandIncorrectDate().send(eventTextChannel);
    }
  }

  private void handleCalendarCommand(TextChannel eventTextChannel, Server messageServer) {
    commandStateResponseService.startGeneratingCalendar().send(eventTextChannel);
    List<Birthday> allUserBirthdayList = birthdayRepositoryPort.findAllEnabled();
    Map<Long, byte[]> userAvatars = findUsersAvatars(messageServer, allUserBirthdayList);

    List<CalendarUser> calendarUserList = buildCalendarUsers(allUserBirthdayList, userAvatars);
    try {
      InputStream calendarInputStream = calendarBuildService.buildCalendar(calendarUserList);
      String calendarFileName = userResourceBundle.getString("generating.calendar.name");
      commandStateResponseService.sendCalendarAsAttachment(calendarFileName, calendarInputStream).send(eventTextChannel);
      calendarInputStream.close();
    } catch (Exception calendarGeneratingException) {
      log.debug("Error during calendar generating", calendarGeneratingException);
      commandStateResponseService.generatingCalendarError().send(eventTextChannel);
    }
  }

  private List<CalendarUser> buildCalendarUsers(List<Birthday> allUserBirthdayList, Map<Long, byte[]> userAvatars) {
    return allUserBirthdayList.stream()
        .map(birthday -> mapToCalendarUser(birthday, userAvatars.get(birthday.getUserId())))
        .collect(Collectors.toList());
  }

  private CalendarUser mapToCalendarUser(Birthday birthday, byte[] imageBytes) {
    return CalendarUser.builder()
        .userId(birthday.getUserId())
        .userName(birthday.getUserNicknameOrName())
        .month(birthday.getMonth())
        .day(birthday.getDay())
        .avatar(imageBytes)
        .build();
  }

  private Map<Long, byte[]> findUsersAvatars(Server messageServer, List<Birthday> allUserBirthdayList) {
    List<Long> allUserIds = allUserBirthdayList.stream()
        .map(Birthday::getUserId)
        .collect(Collectors.toList());

    Map<Long, byte[]> avatarMap = new HashMap<>();

    for (User serverMember : messageServer.getMembers()) {
      if (allUserIds.contains(serverMember.getId())) {
        Optional<byte[]> userAvatar = getUserAvatar(serverMember);
        userAvatar.ifPresent(bufferedImage -> avatarMap.put(serverMember.getId(), bufferedImage));
      }
    }

    return avatarMap;
  }

  private Optional<byte[]> getUserAvatar(User serverMember) {
    CompletableFuture<byte[]> userAvatarBytes = serverMember.getAvatar().asByteArray();
    try {
      return Optional.ofNullable(userAvatarBytes.join());
    } catch (CancellationException | CompletionException getUserAvatarException) {
      log.debug("Failed to fetch user avatar", getUserAvatarException);
      return Optional.empty();
    }
  }

  private boolean isServerUserMessage(MessageCreateEvent messageCreateEvent) {
    return messageCreateEvent.isServerMessage()
        && isMessageAuthorRegularUser(messageCreateEvent);
  }

  private boolean isPrivateAdminServerMessage(MessageCreateEvent messageCreateEvent) {
    return messageCreateEvent.isPrivateMessage()
        && adminCommandService.hasPermission(messageCreateEvent.getMessageAuthor().getId());
  }

  private boolean isMessageAuthorRegularUser(MessageCreateEvent messageCreateEvent) {
    return !messageCreateEvent.getMessageAuthor().isBotUser()
        && (messageCreateEvent.getMessageAuthor().isRegularUser()
        || messageCreateEvent.getMessageAuthor().isServerAdmin());
  }
}
